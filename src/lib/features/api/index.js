import axios from "axios";

const getToken = () => {
  let token = null;

  if (typeof window !== "undefined") {
    try {
      token = localStorage.getItem("user")
        ? JSON.parse(localStorage.getItem("user")).data.data.token
        : null;
    } catch (error) {
      token = null;
    }
    // console.log("Token from local storage:", token);
  }

  return token;
};

const axiosInstance = axios.create({
  validateStatus: (status) => status >= 200 && status < 300,
  withCredentials: false,
});

// const axiosInstance = axios.create({
//   validateStatus() {
//     return true;
//   },
//   withCredentials: false,
// });

const getHeaders = (isFormData = false) => {
  let headers = {
    "Content-type": !isFormData
      ? "application/json"
      : "application/x-www-form-urlencoded",
  };

  const token = getToken();

  if (token) {
    headers = { Authorization: `Bearer ${token}` };
  }

  return { headers };
};

const API = {
  get: (endPoint) => {
    return axiosInstance.get(endPoint, getHeaders());
  },
  post: (endPoint, body, isFormData) => {
    return axiosInstance.post(endPoint, body, getHeaders(isFormData));
  },
  put: (endPoint, body) => {
    return axiosInstance.put(endPoint, body, getHeaders());
  },
  patch: (endPoint, body) => {
    return axiosInstance.patch(endPoint, body, getHeaders());
  },
  delete: (endPoint) => {
    return axiosInstance.delete(endPoint, getHeaders());
  },
};

export default API;
export { getToken, getHeaders };
