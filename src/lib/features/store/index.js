import { configureStore } from "@reduxjs/toolkit";
import colorReducer from "@/lib/features/reducers/colorReducer";
import productReducer from "@/lib/features/reducers/products/productsReducer";
import categoryReducer from "@/lib/features/reducers/category/categoryReducer";
import userReducer from "@/lib/features/reducers/userReducer";
import signReducer from "@/lib/features/reducers/signInReducer";
import personsReducer from "@/lib/features/reducers/register/registerReducer";


const store = configureStore({
  reducer: {
    color: colorReducer,
    product: productReducer,
    category: categoryReducer,
    user: userReducer,
    sign: signReducer,
    register: personsReducer,
  },
});


export default store;
