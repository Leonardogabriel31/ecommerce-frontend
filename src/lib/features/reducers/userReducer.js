import {
  SET_USER_FROM_STORAGE,
  REGISTER_USER_LOGIN,
  LOGIN_PENDING,
  LOGIN_FULFILLED,
  LOGIN_REJECTED,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED,
  SET_EMAIL,
  SET_HELPER_TEXT_EMAIL,
  SET_ERROR_EMAIL,
  SET_ERROR_PASSWORD,
  SET_HELPER_TEXT_PASSWORD,
  SET_PASSWORD,
} from "@/lib/features/types/login/userTypes";

const initialState = {
  email: "",
  helperTextEmail: "",
  errorEmail: null,
  password: "",
  helperTextPassword: "",
  errorPassword: null,
  loading: false,
  user: null,
  error: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_FROM_STORAGE:
      return {
        ...state,
        user: action.payload,
        loading: false,
      };
    case LOGIN_PENDING:
      return {
        ...state,
        loading: true,
        user: null,
        error: null,
      };
    case LOGIN_FULFILLED:
      // console.log(action.payload);
      return {
        ...state,
        loading: false,
        user: action.payload,
        error: null,
      };
    case LOGIN_REJECTED:
      return {
        ...state,
        loading: false,
        user: null,
        error: action.payload,
      };
    case LOGOUT_SUCCESS:
      localStorage.removeItem("user");
      return {
        ...state,
        user: null, // Asignar null a user para indicar que el usuario ha sido deslogueado
        loading: false,
        error: null,
      };
    case LOGOUT_FAILED:
      return {
        ...state,
        loading: false,
        user: null,
        error: action.payload,
      };
    case SET_EMAIL:
      return { ...state, email: action.payload };
    case SET_HELPER_TEXT_EMAIL:
      return { ...state, helperTextEmail: action.payload };
    case SET_ERROR_EMAIL:
      return { ...state, errorEmail: action.payload };
    case SET_PASSWORD:
      return { ...state, password: action.payload };
    case SET_HELPER_TEXT_PASSWORD:
      return { ...state, helperTextPassword: action.payload };
    case SET_ERROR_PASSWORD:
      return { ...state, errorPassword: action.payload };
    default:
      return state;
  }
};

export default userReducer;
