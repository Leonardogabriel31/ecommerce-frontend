import { createSlice } from "@reduxjs/toolkit";

const signSlice = createSlice({
  name: "sign",
  initialState: {
    sign: {},
  },
  reducers: {
    setSignin: (state, action) => {
      state.color = action.payload;
    }
  },
});

export const { setSignin } = signSlice.actions;
export default signSlice.reducer;
