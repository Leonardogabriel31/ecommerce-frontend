import {
  REGISTER_CREATE_USER,
  GET_USERS_ALL,
  DELETE_USER,
  UPDATE_USER,
  SET_EDITING_USER,
  UPDATE_EDITING_USER_FORM,
  SET_ERROR_BACKEND,
  SET_OPEN_SNACKBAR,
  SET_FORM,
  SET_INPUT_VALUE,
  SET_ERROR,
  SET_PASSWORD,
  SET_ERROR_PASSWORD,
  SET_EMAIL,
  SET_ERROR_EMAIL,
  SET_PHONE,
  SET_ERROR_PHONE,
  SET_CODEPOSTAL,
  SET_ERROR_CODEPOSTAL,
  SET_COUNTRY,
  SET_ERROR_COUNTRY,
  SET_SUCCESS_MESSAGE,
  RESET_FORM,
  SET_HELPER_TEXT,
  SET_HELPER_TEXT_EMAIL,
  SET_HELPER_TEXT_PHONE,
  SET_HELPER_TEXT_PASSWORD,
  SET_HELPER_TEXT_COUNTRY,
  SET_HELPER_TEXT_CODEPOSTAL,
} from "@/lib/features/types/register/registerTypes";

const initialState = {
  data: [],
  form: {
    fullName: "",
    password: "",
    email: "",
    phoneNumber: "",
    codepostal: "",
    country: "",
  },

  editingUserForm: {
    fullName: "",
    password: "",
    email: "",
    phoneNumber: "",
    codePostal: "",
    country: "",
  },
  editingUser: null,

  inputValue: "",
  error: null,
  password: "",
  errorPassword: "",
  email: "",
  errorEmail: null,
  phone: "",
  errorPhone: "",
  codepostal: "",
  errorCodepostal: "",
  country: "",
  errorCountry: "",
  successMessage: "",
  helperText: "",
  helperTextEmail: "",
  helperTextPhone: "",
  helperTextPassword: "",
  helperTextCountry: "",
  helperTextCodepostal: "",
  errorBackend: null,
  openSnackbar: false,
};

const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_CREATE_USER:
      return {
        ...state,
        data: [...state.data, action.payload],
        form: action.payload,
        errorBackend: null,
      };
    case GET_USERS_ALL:
      return {
        ...state,
        data: action.payload,
      };
    case DELETE_USER:
      return {
        ...state,
        data: state.data.filter((user) => user.id !== action.payload),
      };

    case UPDATE_USER:
      const updatedUser = action.payload;
      const updatedUsers = state.data.map((user) =>
        user.id === updatedUser.id ? { ...user, ...updatedUser } : user
      );
      return { ...state, data: updatedUsers, editingUser: null };

    case SET_EDITING_USER:
      return {
        ...state,
        editingUser: action.payload,
      };
    case UPDATE_EDITING_USER_FORM:
      return {
        ...state,
        editingUserForm: { ...state.editingUserForm, ...action.payload },
      };

    case SET_SUCCESS_MESSAGE:
      return { ...state, successMessage: action.payload };
    case RESET_FORM:
      return { ...state, form: initialState.form };
    case SET_ERROR_BACKEND:
      return { ...state, form: action.payload };
    case SET_OPEN_SNACKBAR:
      return { ...state, openSnackbar: action.payload };
    case SET_FORM:
      return { ...state, form: action.payload };
    case SET_INPUT_VALUE:
      return {
        ...state,
        form: { ...state.form, [action.payload.name]: action.payload.value },
      };
    case SET_ERROR:
      return { ...state, error: action.payload };
    case SET_PASSWORD:
      return { ...state, form: { ...state.form, password: action.payload } };
    case SET_ERROR_PASSWORD:
      return { ...state, errorPassword: action.payload };
    case SET_EMAIL:
      return { ...state, form: { ...state.form, email: action.payload } };
    case SET_ERROR_EMAIL:
      return { ...state, errorEmail: action.payload };
    case SET_PHONE:
      return { ...state, form: { ...state.form, phone: action.payload } };
    case SET_ERROR_PHONE:
      return { ...state, errorPhone: action.payload };
    case SET_CODEPOSTAL:
      return { ...state, form: { ...state.form, codepostal: action.payload } };
    case SET_ERROR_CODEPOSTAL:
      return { ...state, errorCodepostal: action.payload };
    case SET_COUNTRY:
      return { ...state, form: { ...state.form, country: action.payload } };
    case SET_ERROR_COUNTRY:
      return { ...state, errorCountry: action.payload };
    case SET_HELPER_TEXT:
      return { ...state, helperText: action.payload };
    case SET_HELPER_TEXT_EMAIL:
      return { ...state, helperTextEmail: action.payload };
    case SET_HELPER_TEXT_PHONE:
      return { ...state, helperTextPhone: action.payload };
    case SET_HELPER_TEXT_PASSWORD:
      return { ...state, helperTextPassword: action.payload };
    case SET_HELPER_TEXT_COUNTRY:
      return { ...state, helperTextCountry: action.payload };
    case SET_HELPER_TEXT_CODEPOSTAL:
      return { ...state, helperTextCodepostal: action.payload };

    default:
      return state;
  }
};

export default registerReducer;
