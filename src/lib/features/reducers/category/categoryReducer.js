import {
  SET_FORM,
  GET_CATEGORY_ALL,
  CREATE_CATEGORY,
  DELETE_CATEGORY,
  UPDATE_CATEGORY,
  SET_EDITING_CATEGORY,
  UPDATE_EDITING_CATEGORY_FORM,
} from "@/lib/features/types/category/categoryTypes";

const initialState = {
  categoriesAll: [],
  form: {
    name: "",
  },
  inputValue: "",
  editingCategory: null,
  editingCategoryForm: {
    name: "",
  },
};

const categoryReducer = (state = initialState, action) => {
  if (state === undefined) {
    return initialState;
  }
  switch (action.type) {
    case SET_FORM:
      return { ...state, form: action.payload };
    case GET_CATEGORY_ALL:
      return {
        ...state,
        categoriesAll: action.payload.data,
      };
    case CREATE_CATEGORY:
      return {
        ...state,
        categoriesAll: [...state.categoriesAll, action.payload],
        form: action.payload,
        errorBackend: null,
      };
    case DELETE_CATEGORY:
      return {
        ...state,
        categoriesAll: state.categoriesAll.filter(
          (category) => category.id !== action.payload
        ),
      };
    case UPDATE_CATEGORY:
      const updatedCategories = state.categoriesAll.map((category) =>
        category.id === action.payload.id ? action.payload : category
      );
      return {
        ...state,
        categoriesAll: updatedCategories,
        editingCategory: null,
        categories: updatedCategories,
      };
    case SET_EDITING_CATEGORY:
      return {
        ...state,
        editingCategory: action.payload,
      };
    case UPDATE_EDITING_CATEGORY_FORM:
      return {
        ...state,
        editingCategoryForm: action.payload,
      };
    default:
      return state;
  }
};

export default categoryReducer;
