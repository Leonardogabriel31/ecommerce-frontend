import { createSlice } from "@reduxjs/toolkit";
import clothes from "@/services/clothes";
import technologies from "@/services/technology";
import homeAppliances from "@/services/homeAppliance";
import accessories from "@/services/accessories"

let itemsCart = [];
let quantityTotal = 0;
let priceTotal = 0;

if (typeof window !== "undefined") {
  try {
    itemsCart = localStorage.getItem("cart")
      ? JSON.parse(localStorage.getItem("cart"))
      : [];
  } catch (error) {
    itemsCart = [];
  }
}

if (typeof window !== "undefined") {
  try {
    quantityTotal = localStorage.getItem("totalQuantity")
      ? JSON.parse(localStorage.getItem("totalQuantity"))
      : 0;
  } catch (error) {
    quantityTotal = 0;
  }
}

if (typeof window !== "undefined") {
  try {
    priceTotal = localStorage.getItem("totalPrice")
      ? JSON.parse(localStorage.getItem("totalPrice"))
      : 0;
  } catch (error) {
    priceTotal = 0;
  }
}

const productsSlice = createSlice({
  name: "product",
  initialState: {
    clothe: clothes,
    technology: technologies,
    home: homeAppliances,
    accessory: accessories,
    cart: itemsCart,
    totalQuantity: quantityTotal,
    totalPrice: priceTotal,
  },
  reducers: {
    addToCart: (state, action) => {
      let find = state.cart.findIndex((item) => item.id === action.payload.id);
      if (find >= 0) {
        state.cart[find].quantity += 1;
      } else {
        state.cart.push(action.payload);
      }
      localStorage.setItem("cart", JSON.stringify(state.cart));
      localStorage.setItem(
        "totalQuantity",
        JSON.stringify(state.totalQuantity)
      );
      localStorage.setItem("totalPrice", JSON.stringify(state.totalPrice));
    },

    getCartTotal: (state) => {
      let { totalQuantity, totalPrice } = state.cart.reduce(
        (cartTotal, cartItem) => {
          console.log("carttotal", cartTotal);
          console.log("cartitem", cartItem);
          const { price, quantity } = cartItem;
          console.log(price, quantity);
          const itemTotal = price * quantity;
          cartTotal.totalPrice += itemTotal;
          cartTotal.totalQuantity += quantity;
          return cartTotal;
        },
        {
          totalPrice: 0,
          totalQuantity: 0,
        }
      );
      state.totalPrice = parseInt(totalPrice.toFixed(2));
      state.totalQuantity = totalQuantity;
    },

    removeItem: (state, action) => {
      state.cart = state.cart.filter((item) => item.id !== action.payload);
      localStorage.setItem("cart", JSON.stringify(state.cart));
      localStorage.setItem(
        "totalQuantity",
        JSON.stringify(state.totalQuantity)
      );
      localStorage.setItem("totalPrice", JSON.stringify(state.totalPrice));
    },

    increaseItemQuantity: (state, action) => {
      state.cart = state.cart.map((item) => {
        if (item.id === action.payload) {
          return { ...item, quantity: item.quantity + 1 };
        }
        return item;
      });
    },

    decreaseItemQuantity: (state, action) => {
      state.cart = state.cart.map((item) => {
        if (item.id === action.payload) {
          return { ...item, quantity: item.quantity - 1 };
        }
        return item;
      });
    },

    setOpen: (state, action) => {
      state.color = action.payload;
    },

    setWidth: (state, action) => {
      state.color = action.payload;
    },

    setHeight: (state, action) => {
      state.color = action.payload;
    },
  },
});

export const {
  addToCart,
  getCartTotal,
  removeItem,
  increaseItemQuantity,
  decreaseItemQuantity,
  setOpen,
  setWidth,
  setHeight,
} = productsSlice.actions;
export default productsSlice.reducer;
