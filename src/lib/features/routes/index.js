// const API_SITE_MAIN = "https://ecommerce-django-rest.onrender.com/api";
const API_SITE_MAIN = " https://e-commerce-back-django.onrender.com/api";

const endPoints = {
  auth: {
    login: `${API_SITE_MAIN}/auth/login`,
    logout: `${API_SITE_MAIN}/auth/logout`,
  },
  user: {
    getAll: `${API_SITE_MAIN}/user`,
    delete: (id) => `${API_SITE_MAIN}/user/${id}`,
    update: `${API_SITE_MAIN}/user/`,
  },
  category: {
    getAll: `${API_SITE_MAIN}/category/`,
    create: `${API_SITE_MAIN}/category/`,
    delete: (id) => `${API_SITE_MAIN}/category/${id}`,
    update: (id) => `${API_SITE_MAIN}/category/${id}`,
  },
};

export default endPoints;
