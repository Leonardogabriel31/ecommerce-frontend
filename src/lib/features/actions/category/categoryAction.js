import API from "../../api";
import endPoints from "@/lib/features/routes/index";
import {
  SET_FORM,
  GET_CATEGORY_ALL,
  CREATE_CATEGORY,
  DELETE_CATEGORY,
  UPDATE_CATEGORY,
  SET_EDITING_CATEGORY,
  UPDATE_EDITING_CATEGORY_FORM
} from "@/lib/features/types/category/categoryTypes";

export const setForm = (inputValue) => {
  return { type: SET_FORM, payload: inputValue };
};

export const getCategories = () => async (dispatch) => {
  try {
    const response = await API.get(endPoints.category.getAll);
    dispatch({ type: GET_CATEGORY_ALL, payload: response.data });
  } catch (error) {
    console.error("Error obteniendo usuarios:", error);
    throw error;
  }
};

export const createCategory = (form) => async (dispatch) => {
  try {
    const response = await API.post(endPoints.category.create, form);
    dispatch({ type: CREATE_CATEGORY, payload: response.data });
  } catch (error) {
    console.error("Error creando categoría:", error);
    throw error;
  }
};

export const deleteCategory = (id) => async (dispatch) => {
  try {
    await API.delete(endPoints.category.delete(id));
    dispatch({ type: DELETE_CATEGORY, payload: id });
    dispatch(getCategories());
  } catch (error) {
    console.error("Error eliminando categoría:", error);
    throw error;
  }
};

export const updateCategory = (id, form) => async (dispatch) => {
  try {
    const response = await API.put(endPoints.category.update(id), form);
    dispatch({ type: UPDATE_CATEGORY, payload: response.data });
    // console.log(response.data)
  } catch (error) {
    console.error("Error updating category:", error);
    throw error;
  }
};

export const setEditingCategory = (category) => {
  return { type: SET_EDITING_CATEGORY, payload: category };
};

export const updateEditingCategoryForm = (updates) => {
  return { type: UPDATE_EDITING_CATEGORY_FORM, payload: updates };
};