import {
  REGISTER_CREATE_USER,
  GET_USERS_ALL,
  DELETE_USER,
  SET_INPUT_VALUE,
  SET_ERROR,
  SET_PASSWORD,
  SET_ERROR_PASSWORD,
  SET_EMAIL,
  SET_ERROR_EMAIL,
  SET_PHONE,
  SET_ERROR_PHONE,
  SET_CODEPOSTAL,
  SET_ERROR_CODEPOSTAL,
  SET_COUNTRY,
  SET_ERROR_COUNTRY,
  SET_FORM,
  SET_SUCCESS_MESSAGE,
  RESET_FORM,
  SET_HELPER_TEXT,
  SET_HELPER_TEXT_EMAIL,
  SET_HELPER_TEXT_PHONE,
  SET_HELPER_TEXT_PASSWORD,
  SET_HELPER_TEXT_COUNTRY,
  SET_HELPER_TEXT_CODEPOSTAL,
  UPDATE_USER,
  SET_EDITING_USER,
  UPDATE_EDITING_USER_FORM,
} from "@/lib/features/types/register/registerTypes";
import axios from "axios";
import API, { getHeaders, getToken } from "../../api";
import endPoints from "@/lib/features/routes/index";
import { SET_OPEN_SNACKBAR } from "../../types/register/registerTypes";

export const createUser = (form) => {
  return async (dispatch) => {
    try {
      const response = await axios.post(
        "https://ecommerce-django-rest.onrender.com/api/user/",
        form
      );
      dispatch({ type: REGISTER_CREATE_USER, payload: response.data });
      dispatch({ type: SET_SUCCESS_MESSAGE, payload: response.data.msg });
      console.log(response.data.msg);
      return { type: REGISTER_CREATE_USER, payload: response.data };
    } catch (error) {
      const errorMessage = error.response.data;
      if (errorMessage.errors) {
        if (errorMessage.errors.person.fullName) {
          dispatch(setHelperText(errorMessage.errors.person.fullName));
          dispatch(setError(true));
        }
        if (errorMessage.errors.email) {
          dispatch(setHelperTextEmail(errorMessage.errors.email));
          dispatch(setErrorEmail(true));
        }
        if (errorMessage.errors.person.phoneNumber) {
          dispatch(setHelperTextPhone(errorMessage.errors.person.phoneNumber));
          dispatch(setErrorPhone(true));
        }
        if (errorMessage.errors.password) {
          dispatch(setHelperTextPassword(errorMessage.errors.password));
          dispatch(setErrorPassword(true));
        }
        if (errorMessage.errors.person.country) {
          dispatch(setHelperTextCountry(errorMessage.errors.person.country));
          dispatch(setErrorCountry(true));
        }
        if (errorMessage.errors.person.codePostal) {
          dispatch(
            setHelperTextCodepostal(errorMessage.errors.person.codePostal)
          );
          dispatch(setErrorCodepostal(true));
        }
      }
      dispatch({ type: SET_ERROR, payload: errorMessage });
      throw error;
    }
  };
};

export const getUsers = () => async (dispatch) => {
  try {
    const response = await API.get(endPoints.user.getAll);
    dispatch({ type: GET_USERS_ALL, payload: response.data });
    // console.log(response)
  } catch (error) {
    console.error("Error obteniendo usuarios:", error);
    throw error;
  }
};

export const deleteUsers = (id) => async (dispatch) => {
  try {
    await API.delete(endPoints.user.delete(id));
    dispatch({ type: DELETE_USER, payload: id });
    dispatch(getUsers());
  } catch (error) {
    console.error("Error eliminando categoría:", error);
    throw error;
  }
};

export const updateUser = (updatedUser) => async (dispatch) => {
  console.log(updatedUser);
  try {
    const response = await API.patch(endPoints.user.update, updatedUser);
    dispatch({ type: UPDATE_USER, payload: response.data });
    localStorage.setItem("user", JSON.stringify(response));
    // console.log(response.data);
  } catch (error) {
    console.error("Error updating user:", error);
  }
};

export const setEditingUser = (user) => {
  return { type: SET_EDITING_USER, payload: user };
};

export const updateEditingUserForm = (updates) => {
  return { type: UPDATE_EDITING_USER_FORM, payload: updates };
};

export const resetForm = () => {
  return { type: RESET_FORM };
};

export const setForm = (inputValue) => {
  return { type: SET_FORM, payload: inputValue };
};

export const setOpenSnackbar = (openSnackbar) => {
  return { type: SET_OPEN_SNACKBAR, payload: openSnackbar };
};

export const setInputValue = (name, value) => {
  return { type: SET_INPUT_VALUE, payload: { name, value } };
};

export const setError = (error) => {
  return { type: SET_ERROR, payload: error };
};

export const setPassword = (password) => {
  return { type: SET_PASSWORD, payload: password };
};

export const setErrorPassword = (error) => {
  return { type: SET_ERROR_PASSWORD, payload: error };
};

export const setEmail = (email) => {
  return { type: SET_EMAIL, payload: email };
};

export const setErrorEmail = (errorEmail) => {
  return { type: SET_ERROR_EMAIL, payload: errorEmail };
};

export const setPhone = (phone) => {
  return { type: SET_PHONE, payload: phone };
};

export const setErrorPhone = (error) => {
  return { type: SET_ERROR_PHONE, payload: error };
};

export const setCodepostal = (codepostal) => {
  return { type: SET_CODEPOSTAL, payload: codepostal };
};

export const setErrorCodepostal = (error) => {
  return { type: SET_ERROR_CODEPOSTAL, payload: error };
};

export const setCountry = (country) => {
  return { type: SET_COUNTRY, payload: country };
};

export const setErrorCountry = (error) => {
  return { type: SET_ERROR_COUNTRY, payload: error };
};

export const setHelperText = (helperText) => {
  return { type: SET_HELPER_TEXT, payload: helperText };
};

export const setHelperTextEmail = (helperTextEmail) => {
  return { type: SET_HELPER_TEXT_EMAIL, payload: helperTextEmail };
};

export const setHelperTextPhone = (helperTextPhone) => {
  return { type: SET_HELPER_TEXT_PHONE, payload: helperTextPhone };
};

export const setHelperTextPassword = (helperTextPassword) => {
  return { type: SET_HELPER_TEXT_PASSWORD, payload: helperTextPassword };
};

export const setHelperTextCountry = (helperTextCountry) => {
  return { type: SET_HELPER_TEXT_COUNTRY, payload: helperTextCountry };
};

export const setHelperTextCodepostal = (helperTextCodepostal) => {
  return { type: SET_HELPER_TEXT_CODEPOSTAL, payload: helperTextCodepostal };
};
