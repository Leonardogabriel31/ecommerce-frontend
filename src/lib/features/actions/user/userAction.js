  import axios from "axios";
  import { useAppDispatch, useAppSelector } from "../../hooks/hooks";
  import API, { getHeaders, getToken } from "../../api";
  import endPoints from "@/lib/features/routes/index";
  import {
    SET_USER_FROM_STORAGE,
    LOGIN_PENDING,
    LOGIN_FULFILLED,
    LOGIN_REJECTED,
    LOGOUT_SUCCESS,
    LOGOUT_FAILED,
    SET_EMAIL,
    SET_HELPER_TEXT_EMAIL,
    SET_ERROR_EMAIL,
    SET_PASSWORD,
    SET_HELPER_TEXT_PASSWORD,
    SET_ERROR_PASSWORD,
    REGISTER_USER_LOGIN,
  } from "@/lib/features/types/login/userTypes";

  export const setUserFromStorage = (userData) => ({
    type: SET_USER_FROM_STORAGE,
    payload: userData,
  });


  export function login(email, password) {
    return async (dispatch) => {
      dispatch({ type: LOGIN_PENDING });
      try {
        const response = await axios.post(
          "https://e-commerce-back-django.onrender.com/api/auth/login",
          {
            email,
            password,
          }
        );
        dispatch({ type: LOGIN_FULFILLED, payload: response.data.data });
        localStorage.setItem("user", JSON.stringify(response));
        getToken();
        return response.data.data;
      } catch (error) {
        const errorMessage = error.response.data;
        console.log(errorMessage);
        if (errorMessage) {
          if (errorMessage.msg) {
            dispatch(setHelperTextEmail(errorMessage.msg));
            dispatch(setErrorEmail(true));
          }
        }
        dispatch({ type: LOGIN_REJECTED, payload: error.message });
        throw error;
      }
    };
  }

  export const logout = () => async (dispatch) => {
    try {
      const headers = getHeaders();
      const response = await API.post(endPoints.auth.logout, {}, headers);
      const logoutAction = { type: LOGOUT_SUCCESS, payload: response.data };
      dispatch(logoutAction);
      localStorage.removeItem("user");
    } catch (error) {
      dispatch({ type: LOGOUT_FAILED, payload: error.message });
      console.error("Error logging out:", error);
    }
  };

  export const useUserData = () => {
    const user = useAppSelector((state) => state.user.user);
    let initialUser = null;
    if (typeof window !== "undefined") {
      try {
        initialUser = localStorage.getItem("user")
          ? JSON.parse(localStorage.getItem("user"))
          : null;

        return user || initialUser.data.data;
      } catch (error) {
        initialUser = null;
      }
    }
  };

  export const setEmail = (email) => {
    return { type: SET_EMAIL, payload: email };
  };

  export const setHelperTextEmail = (helperTextEmail) => {
    return { type: SET_HELPER_TEXT_EMAIL, payload: helperTextEmail };
  };

  export const setErrorEmail = (errorEmail) => {
    return { type: SET_ERROR_EMAIL, payload: errorEmail };
  };

  export const setPassword = (password) => {
    return { type: SET_PASSWORD, payload: password };
  };

  export const setHelperTextPassword = (helperTextPassword) => {
    return { type: SET_HELPER_TEXT_PASSWORD, payload: helperTextPassword };
  };

  export const setErrorPassword = (errorPassword) => {
    return { type: SET_ERROR_PASSWORD, payload: errorPassword };
  };
