const homeAppliances = [
  {
    id: 1,
    img: "https://http2.mlstatic.com/D_NQ_NP_718012-MLV76578822400_062024-O.webp",
    title: "Lavadora Mabe Semi Automática 16 Kg",
    price: 700,
    quantity: 1,
  },
  {
    id: 2,
    img: "https://http2.mlstatic.com/D_NQ_NP_657297-MLV75804769838_042024-O.webp",
    title: "Lavadora Secadora 12 Kilo Daewoo 110v",
    price: 700,
    quantity: 1,
  },
  {
    id: 3,
    img: "https://http2.mlstatic.com/D_NQ_NP_822742-MLV75279590320_032024-O.webp",
    title: "Mini Lavadora Portatil Secadora 8 Litros",
    price: 700,
    quantity: 1,
  },
  {
    id: 4,
    img: "https://http2.mlstatic.com/D_NQ_NP_758406-MLV48099147893_112021-O.webp",
    title: "Lavadora Automática Digital 20 Kg 7 Ciclos Mabe",
    price: 700,
    quantity: 1,
  },
  {
    id: 5,
    img: "https://http2.mlstatic.com/D_NQ_NP_796501-MLV76392866479_052024-O.webp",
    title: "Nevera 12 Pies Con Dispensador Daewo",
    price: 500,
    quantity: 1,
  },
  {
    id: 6,
    img: "https://http2.mlstatic.com/D_NQ_NP_954454-MLV72957485575_112023-O.webp",
    title: "Nevera 11 Pies Marca Aiwa",
    price: 500,
    quantity: 1,
  },

  {
    id: 7,

    img: "https://http2.mlstatic.com/D_NQ_NP_827465-MLV72921331641_112023-O.webp",
    title: "Nevera Galanz 7 Pies Roja",
    price: 500,
    quantity: 1,
  },

  {
    id: 8,

    img: "https://http2.mlstatic.com/D_NQ_NP_610472-MLV70398741726_072023-O.webp",
    title: "Refrigerador Frigilux",
    price: 500,
    quantity: 1,
  },
  {
    id: 9,
    img: "https://http2.mlstatic.com/D_NQ_NP_892048-MLV72998119365_112023-O.webp",
    title: "Cocina Lotus A Gas Alta De Mesa Con 4 Hornillas",
    price: 150,
    quantity: 1,
  },
  {
    id: 10,
    img: "https://http2.mlstatic.com/D_NQ_NP_612526-MLA76153434224_052024-O.webp",
    title: "Cocina A Gas General Plus 20 Pulgada Con Encendido Eléctrico",
    price: 150,
    quantity: 1,
  },

  {
    id: 11,
    img: "https://http2.mlstatic.com/D_NQ_NP_891057-MLA75974868661_042024-O.webp",
    title: "Cocina 4 Hornillas Rca De Lujo Con Encendido Electrico Y Luz",
    price: 150,
    quantity: 1,
  },
  {
    id: 12,
    img: "https://http2.mlstatic.com/D_NQ_NP_868978-MLV72438971175_102023-O.webp",
    title: "Reverbero A Gas 3 Hornilla Cocina Gas Quemador Fogon",
    price: 150,
    quantity: 1,
  },
];

export default homeAppliances;
