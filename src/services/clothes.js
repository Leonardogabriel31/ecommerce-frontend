const clothes = [
  {
    id: 1,
    img: "https://media.istockphoto.com/id/1323480736/es/foto/camiseta-negra.jpg?s=612x612&w=0&k=20&c=lKVbwSFMUEzGQgcyPh4_vGvxZ7M80mKbPyLQfgDjaNM=",
    title: "franela Negra",
    price: 50,
    quantity: 1,
  },
  {
    id: 2,
    img: "https://media.istockphoto.com/id/1323480814/es/foto/gris-mezcla-camiseta.jpg?s=612x612&w=0&k=20&c=I8cSg-Sp3S1GBT4dLW0McKqyRVH0kay0deOX-YJInF8=",
    title: "franela gris",
    price: 50,
    quantity: 1,
  },
  {
    id: 3,
    img: "https://media.istockphoto.com/id/682695702/es/foto/conjunto-de-camisetas-aislado-sobre-fondo-blanco.jpg?s=612x612&w=0&k=20&c=AIbT5mLlIgdKQZsJsU8ITvN-63jWv_ztnB9g5ZkfB0Y=",
    title: "franela azul",
    price: 50,
    quantity: 1,
  },
  {
    id: 4,
    img: "https://media.istockphoto.com/id/1349354246/es/foto/conjunto-con-elegantes-camisetas-sobre-fondo-blanco-dise%C3%B1o-de-banners.jpg?s=612x612&w=0&k=20&c=gNW2NLtMa4tbWBYpeawQ0FkZV4692o0vL9sL-w8Od_8=",
    title: "franela blanca",
    price: 50,
    quantity: 1,
  },
  {
    id: 5,
    img: "https://media.istockphoto.com/id/1406303919/es/foto/pantalones-de-ch%C3%A1ndal-femeninos-sport.jpg?s=612x612&w=0&k=20&c=3ZKHXEqA_y6fJCTWS5UaCx2cfyV-_h57ecA4e8sLeK4=",
    title: "pantalon rojo",
    price: 80,
    quantity: 1,
  },
  {
    id: 6,
    img: "https://media.istockphoto.com/id/1406303918/es/foto/pantalones-de-ch%C3%A1ndal-femeninos-sport.jpg?s=612x612&w=0&k=20&c=Zb1Rqaz-7qmOkTZdGDyLAC9QEGdrGhGe7ZNOFEZ8otk=",
    title: "pantalon blanco",
    price: 80,
    quantity: 1,
  },

  {
    id: 7,

    img: "https://media.istockphoto.com/id/1406303916/es/foto/pantalones-de-ch%C3%A1ndal-femeninos-sport.jpg?s=612x612&w=0&k=20&c=4ojg5BK0MwBF_6EUiLdek_SVIawHF97j6IAbqCzgK3k=",
    title: "pantalon azul",
    price: 80,
    quantity: 1,
  },

  {
    id: 8,

    img: "https://media.istockphoto.com/id/535716997/es/vector/hombres-sweatpants-negro.jpg?s=612x612&w=0&k=20&c=pMhst6FGWl_kG_hQpjQEwzGuFw0SGUoI_1aJ5lgaVhw=",
    title: "pantalon gris",
    price: 80,
    quantity: 1,
  },
  {
    id: 9,
    img: "https://media.istockphoto.com/id/1217970889/es/foto/hermoso-vestido-rojo-femenino-sin-mangas-aisladas-en-blanco-vestido-de-noche.jpg?s=612x612&w=0&k=20&c=NBdx0vdRO3wVAQDiM-6tF2qhE8rz8f5kO4mpX89NSNY=",
    title: "vestido rojo",
    price: 100,
    quantity: 1,
  },
  {
    id: 10,
    img: "https://media.istockphoto.com/id/492462644/es/foto/vestido-verde-con-cinta.jpg?s=612x612&w=0&k=20&c=j6OQ7Ojeco1XSVbYVAVyD2BM5ISVhT8zYPS9pRGl-dc=",
    title: "vestido verde",
    price: 100,
    quantity: 1,
  },

  {
    id: 11,
    img: "https://media.istockphoto.com/id/1389148787/es/foto/vestido-floral-de-seda-de-verano-sobre-fondo-blanco-toma-de-estudio.jpg?s=612x612&w=0&k=20&c=FVuRmxQeaJPRoRk9-G5lsxVY8IgVb482cHImIKDb3Wc=",
    title: "vestido verano",
    price: 100,
    quantity: 1,
  },
  {
    id: 12,
    img: "https://media.istockphoto.com/id/462555507/photo/wedding-dresses-cutout.webp?b=1&s=170667a&w=0&k=20&c=Wp5YahvKUTUBppO0Tl9ymGCNjaKKd__IysVFjCuZM0w=",
    title: "vestido blanco",
    price: 100,
    quantity: 1,
  },
];

export default clothes;
