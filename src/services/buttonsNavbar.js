const settings = [
  { name: "home", href: "/" },
  { name: "clothes", href: "/categories/clothesTotal" },
  { name: "technology", href: "/categories/tecnology" },
  { name: "homeAppliances", href: "/categories/homeAppliances" },
  { name: "accessories", href: "/categories/accesories" },
];

export default settings;
