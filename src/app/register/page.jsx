import React, { Fragment } from "react";
import RegisterComponent from "@/components/register/registerComponent";

function Register() {
  return (
    <Fragment>
      <RegisterComponent />
    </Fragment>
  );
}

export default Register;
