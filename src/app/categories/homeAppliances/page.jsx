import React, { Fragment } from "react";
import HomeApplianceTotalComponent from "@/components/homeApplianceComponent/homeApplianceTotalComponent";

function ClothesTotal() {
  return (
    <Fragment>
      <HomeApplianceTotalComponent />
    </Fragment>
  );
}

export default ClothesTotal;
