import React, { Fragment } from "react";
import AccessoriesTotalComponent from "@/components/accesoriesComponent/accesoriesTotalComponent";

function AccesoriesPage() {
  return (
    <Fragment>
      <AccessoriesTotalComponent />
    </Fragment>
  );
}

export default AccesoriesPage;
