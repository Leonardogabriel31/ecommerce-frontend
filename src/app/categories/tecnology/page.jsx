import React, { Fragment } from "react";
import TechnologiesTotalComponent from "@/components/technologiesComponent/technologycomponent";

function TechnoPage() {
  return (
    <Fragment>
      <TechnologiesTotalComponent />
    </Fragment>
  );
}

export default TechnoPage;
