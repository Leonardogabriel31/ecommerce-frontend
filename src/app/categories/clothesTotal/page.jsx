import React, { Fragment } from 'react'
import ClothesTotalComponent from "@/components/clothesComponent/clothesTotalComponent"

function ClothesTotal() {
  return (
    <Fragment>
      <ClothesTotalComponent />
    </Fragment>
  )
}

export default ClothesTotal
