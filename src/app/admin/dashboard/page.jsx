import React, { Fragment } from "react";
import ContainerDashboard from "@/components/principalContainer/containerAdmin/containerDashboard";

function Page() {
  return (
    <Fragment>
      <ContainerDashboard />
    </Fragment>
  );
}

export default Page;
