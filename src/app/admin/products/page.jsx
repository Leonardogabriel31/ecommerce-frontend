import React from 'react'
import { Fragment } from 'react'
import ContainerProducts from "@/components/principalContainer/containerAdmin/containerProducts"

function Products() {
  return (
    <Fragment>
      <ContainerProducts />
    </Fragment>
  )
}

export default Products
