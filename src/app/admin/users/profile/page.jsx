import React, { Fragment } from "react";
// import ProfileComponent from '../../../components/principalContainer/containerAdmin/componentsContainerAdmin/users/profile/profileComponent';
import ProfileComponent from "@/components/principalContainer/containerAdmin/componentsContainerAdmin/users/profile/profileComponent";

function Profile() {
  return (
    <Fragment>
      <ProfileComponent />
    </Fragment>
  );
}

export default Profile;
