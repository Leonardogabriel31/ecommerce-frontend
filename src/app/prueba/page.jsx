"use client";

import { Box } from "@mui/material";
import React from "react";
import PruebaComponent from '../../components/prueba/pruebaComponent'

function Prueba() {
  return (
    <Box
      component={"div"}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <PruebaComponent />
    </Box>
  );
}

export default Prueba;
