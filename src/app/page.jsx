"use client";
import ContainerHome from "@/components/principalContainer/containerHome";
import { Fragment, useEffect } from "react";
import { useAppDispatch } from "@/lib/features/hooks/hooks";
import { changeColor } from "@/lib/features/reducers/colorReducer";
import Navbar from "@/components/navbar/navbar";
import Footer from "@/components/footer/footer";
import { Box } from "@mui/material";

export default function Home() {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(changeColor("white"));
  }, [dispatch]);

  return (
    <Fragment>
      <Box>
        <Navbar />
        <ContainerHome />
        <Footer />
      </Box>
    </Fragment>
  );
}
