import React, { Fragment } from 'react'
import LoginComponent from '@/components/login/loginComponent'
import { Box } from '@mui/material'
import Background from "@/components/particles/background"

function Login() {
  return (
    <Box component={"div"}>
      <Background />
      <LoginComponent />
    </Box>
  )
}

export default Login
