"use client";
import React, { Fragment, forwardRef, useState, useEffect } from "react";
import {
  decreaseItemQuantity,
  getCartTotal,
  increaseItemQuantity,
  removeItem,
} from "../../lib/features/reducers/products/productsReducer";
import { useAppDispatch, useAppSelector } from "@/lib/features/hooks/hooks";
import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";
import Slide from "@mui/material/Slide";
import {
  Box,
  Button,
  Card,
  CardContent,
  Dialog,
  Typography,
  Grid,
  Fab,
} from "@mui/material";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import RemoveIcon from "@mui/icons-material/Remove";
import LocalMallIcon from "@mui/icons-material/LocalMall";
import Image from "next/image";

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function CartBuyComponent({ close }) {
  const [open, setOpen] = useState(false);
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  const { cart, totalQuantity, totalPrice } = useAppSelector(
    (state) => state.product
  );

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getCartTotal());
    console.log(cart);
  }, [cart, dispatch]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    close();
  };

  return (
    <Fragment>
      <Box component={"div"}>
        <Fab
          aria-label="cart"
          onClick={handleClickOpen}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
          style={{
            display: "flex",
            fontWeight: "bold",
            color: "rgb(250, 250, 250)",
            backgroundColor: isHovered
              ? "rgb(60, 60, 60)"
              : "rgb(100, 100, 100)",
            fontSize: isHovered ? 25 : 13,
            fontWeight: "uppercase",
            padding: 1,
            paddingRight: 2,
            width: isHovered ? 110 : 90,
            height: isHovered ? 110 : 90,
            transition: "all ease 0.4s",
          }}
        >
          <ShoppingCartIcon sx={{ fontSize: 40 }} />
        </Fab>
      </Box>
      <Dialog
        keepMounted
        fullWidth={true}
        maxWidth={"xl"}
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
        aria-describedby="alert-dialog-slide-description"
      >
        <Box
          component={"div"}
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            alignItems: "center",
            padding: 2,
            paddingTop: 3,
          }}
        >
          <CloseIcon onClick={handleClose} sx={{ cursor: "pointer" }} />
        </Box>
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
          padding={2}
          paddingBottom={8}
          marginTop="10px"
          gap={4}
          spacing={2}
          columns={{ xs: 1, sm: 1, md: 1, lg: 1, xl: 1 }}
        >
          <Grid
            item
            xs={3}
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "center",
              paddingRight: 14,
            }}
          >
            <Box component={"div"}>
              <Card
                sx={{
                  paddingTop: 3,
                  maxWidth: 360,
                  minWidth: 360,
                  maxHeight: 500,
                  minHeight: 180,
                }}
              >
                <CardContent sx={{ textAligne: "justify" }}>
                  <Box
                    component="div"
                    sx={{
                      paddingLeft: 2,
                    }}
                  >
                    <Typography variant={"h5"}>
                      Cantidad total: {totalQuantity}
                    </Typography>
                    <Typography variant={"h5"}>
                      Monto total: {totalPrice}$
                    </Typography>
                  </Box>
                  <Box
                    component={"div"}
                    sx={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      paddingTop: 2,
                    }}
                  >
                    <Button
                      sx={{
                        display: "flex",
                        fontWeight: "bold",
                        color: "rgb(60, 60, 60)",
                        fontSize: 15,
                        fontWeight: "uppercase",
                        padding: 1,
                        paddingRight: 2,
                        width: "80%",
                        gap: 1,
                        "&:hover": {
                          color: "rgb(200, 200, 200)",
                          backgroundColor: "rgb(60, 60, 60)",
                        },
                      }}
                    >
                      <LocalMallIcon />
                      Pagar
                    </Button>
                  </Box>
                </CardContent>
              </Card>
            </Box>
          </Grid>
          {cart?.map((data, index) => (
            <Box component={"div"} key={index} sx={{ paddingTop: 3 }}>
              <Grid
                item
                xs={3}
                sx={{
                  display: "flex",
                  gap: 4,
                }}
              >
                <Box variant="div">
                  <Card
                    key={data.id}
                    sx={{
                      maxWidth: 300,
                      minWidth: 300,
                      maxHeight: 450,
                      minHeight: 310,
                    }}
                  >
                    <Image
                      height={140}
                      width={360}
                      style={{ minHeight: 200, maxHeight: 200 }}
                      src={data.img}
                      alt={data.title}
                    />
                    <CardContent sx={{ textAligne: "justify" }}>
                      <Typography
                        variant="h5"
                        component="div"
                        sx={{
                          fontSize: 17,
                          paddingBottom: 2,
                          paddingLeft: 2,
                        }}
                      >
                        {data.title}
                      </Typography>
                      <Typography
                        component="div"
                        sx={{
                          fontSize: 15,
                          paddingLeft: 2,
                        }}
                      >
                        {data.price}
                      </Typography>

                      <Typography
                        variant="h5"
                        component="div"
                        sx={{ fontSize: 17, paddingLeft: 2, paddingTop: 2 }}
                      >
                        cantidad
                      </Typography>
                      <Box
                        component={"div"}
                        sx={{
                          paddingBottom: 2,
                          display: "inline-flex",
                          justifyContent: "flex-start",
                          alignItems: "center",
                        }}
                      >
                        <Typography
                          component={"div"}
                          sx={{
                            fontSize: 15,
                            paddingLeft: 2,
                          }}
                        >
                          {data.quantity}
                        </Typography>

                        <Box
                          component={"div"}
                          sx={{
                            display: "flex",
                            paddingLeft: 2,
                            gap: 2,
                          }}
                        >
                          <RemoveIcon
                            onClick={() =>
                              dispatch(decreaseItemQuantity(data.id))
                            }
                            sx={{
                              cursor: "pointer",
                              "&:hover": {
                                color: "rgb(260, 260, 260)",
                                backgroundColor: "rgb(100, 100, 100)",
                              },
                            }}
                          />
                          <AddIcon
                            onClick={() =>
                              dispatch(increaseItemQuantity(data.id))
                            }
                            sx={{
                              cursor: "pointer",
                              "&:hover": {
                                color: "rgb(260, 260, 260)",
                                backgroundColor: "rgb(100, 100, 100)",
                              },
                            }}
                          />
                        </Box>
                      </Box>
                    </CardContent>
                    <Box
                      component={"div"}
                      sx={{
                        padding: 1,
                        paddingBottom: 2,
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <Button
                        sx={{
                          display: "flex",
                          fontWeight: "bold",
                          color: "rgb(60, 60, 60)",
                          fontSize: 15,
                          fontWeight: "uppercase",
                          width: "80%",
                          "&:hover": {
                            color: "rgb(200, 200, 200)",
                            backgroundColor: "rgb(60, 60, 60)",
                          },
                        }}
                        onClick={() => dispatch(removeItem(data.id))}
                      >
                        Eliminar
                      </Button>
                    </Box>
                  </Card>
                </Box>
              </Grid>
            </Box>
          ))}
        </Grid>
      </Dialog>
    </Fragment>
  );
}

export default CartBuyComponent;
