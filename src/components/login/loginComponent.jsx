"use client";
import React, { Fragment, useState } from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  Divider,
  Snackbar,
  TextField,
  Typography,
  styled,
} from "@mui/material";
import { useAppDispatch, useAppSelector } from "@/lib/features/hooks/hooks";
import { useRouter } from "next/navigation";
import {
  login,
  setErrorEmail,
  setErrorPassword,
  setHelperTextEmail,
  setHelperTextPassword,
  setPassword,
} from "@/lib/features/actions/user/userAction";
import Footer from "../footer/footer";
import { setEmail } from "@/lib/features/actions/user/userAction";

const Root = styled("div")(({ theme }) => ({
  width: "100%",
  ...theme.typography.body2,
  color: theme.palette.text.secondary,
  "& > :not(style) ~ :not(style)": {
    marginTop: theme.spacing(2),
  },
}));

function LoginComponent() {
  const email = useAppSelector((state) => state.user.email);
  const errorEmail = useAppSelector((state) => state.user.errorEmail);
  const password = useAppSelector((state) => state.user.password);
  const errorPassword = useAppSelector((state) => state.user.errorPassword);
  const [open, setOpen] = useState(false);
  const loading = useAppSelector((state) => state.user.loading);
  const helperTextEmail = useAppSelector((state) => state.user.helperTextEmail);
  const helperTextPassword = useAppSelector(
    (state) => state.user.helperTextPassword
  );
  const error = useAppSelector((state) => state.user.error);
  const router = useRouter();
  const dispatch = useAppDispatch();

  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      const response = await dispatch(login(email, password));
      let userRol = null;
      if (response) {
        userRol = response.user.role;
        setErrorEmail("");
        setPassword("");
        if (userRol === "CLIENT") {
          router.push("/");
        } else if (userRol === "ADMIN") {
          router.push("/admin/dashboard");
        } else if (userRol === "SUPER_ADMIN") {
          router.push("/admin/dashboard");
        }
      }
    } catch (error) {
      if (error) {
        setOpen(true);
      }
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const handleChange = (e) => {
    dispatch(setEmail(e.target.value));
    if (e.target.value.length > 50) {
      dispatch(
        setHelperTextEmail("It should not be more than 50 characters allowed")
      );
      dispatch(setErrorEmail(true));
    } else {
      dispatch(setErrorEmail(false));
    }
    if (e.target.value.length < 4) {
      dispatch(
        setHelperTextEmail("Must not be less than 4 characters allowed")
      );
      dispatch(setErrorEmail(true));
    } else {
      setErrorEmail(false);
    }
  };

  // const handleChange = (e) => {
  //   dispatch(setEmail(e.target.value));
  //   if (e.target.value.length > 50) {
  //     dispatch(
  //       setHelperTextEmail("It should not be more than 50 characters allowed")
  //     );
  //     dispatch(setErrorEmail(true));
  //   } else if (e.target.value.length < 4) {
  //     dispatch(
  //       setHelperTextEmail("Must not be less than 4 characters allowed")
  //     );
  //     dispatch(setErrorEmail(true));
  //   } else {
  //     setErrorEmail(false);
  //   }
  // };

  const handleChangePassword = (e) => {
    dispatch(setPassword(e.target.value));
    if (e.target.value.length < 8) {
      dispatch(
        setHelperTextPassword("Must not be less than 8 characters allowed")
      );
      dispatch(setErrorPassword(true));
    } else {
      dispatch(setErrorPassword(false));
    }
  };

  return (
    <Fragment>
      <Box component={"div"}>
        <Box
          component={"div"}
          sx={{
            marginTop: "24vh",
            display: "grid",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
            marginBottom: "3.7vh",
          }}
        >
          <Box component={"div"}>
            <Typography
              component="div"
              sx={{
                flexGrow: 1,
                fontSize: 30,
                color: "#fafbfd",
                paddingBottom: 2,
                paddingTop: 1,
                // display: "block"
              }}
            >
              Hola mundo
            </Typography>
          </Box>
          <Card
            sx={{
              backgroundColor: "#18171c",
              width: 450,
              maxHeight: 390,
              display: "block",
            }}
          >
            <Typography
              component="div"
              sx={{
                flexGrow: 1,
                fontSize: 26,
                color: "#fafbfd",
                paddingTop: 3,
                paddingRight: 20,
              }}
            >
              Iniciar Sesión
            </Typography>
            <CardContent
              sx={{
                textAligne: "justify",
                display: "flow",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Box
                component={"div"}
                sx={{
                  display: "flow",
                  justifyContent: "center",
                  alignItems: "center",
                  paddingBottom: 2,
                  padding: 3,
                  paddingLeft: 4,
                  paddingRight: 4,
                }}
              >
                <form>
                  <TextField
                    required
                    // defaultValue={''}
                    // autoComplete="new-password"
                    type="text"
                    label="Correo Electronico"
                    variant="outlined"
                    value={email}
                    onChange={handleChange}
                    error={errorEmail}
                    helperText={errorEmail ? helperTextEmail : ""}
                    inputProps={{
                      style: {
                        color: "#fafbfd", // Add this line
                      },
                    }}
                    sx={{
                      backgroundColor: "#4e4e4e",
                      color: "#fff",
                      width: "100%",
                      marginBottom: 4,
                      "& .MuiInputLabel-root": {
                        // Add this line
                        color: "#fafbfd", // Change the label color to #fafbfd
                      },
                      "& .MuiOutlinedInput-notchedOutline": {
                        // Add this line
                        borderColor: "white", // Change the border color to white
                      },
                    }}
                  />
                  <TextField
                    required
                    // defaultValue={''}
                    autoComplete="new-password"
                    type="password"
                    label="Contraseña"
                    variant="outlined"
                    value={password}
                    error={errorPassword}
                    helperText={errorPassword ? helperTextPassword : ""}
                    onChange={handleChangePassword}
                    inputProps={{
                      style: {
                        color: "#fafbfd", // Add this line
                      },
                    }}
                    sx={{
                      // display: "block",
                      backgroundColor: "#4e4e4e",
                      color: "#fff",
                      width: "100%",
                      "& .MuiInputLabel-root": {
                        // Add this line
                        color: "#fafbfd", // Change the label color to #fafbfd
                      },
                      "& .MuiOutlinedInput-notchedOutline": {
                        // Add this line
                        borderColor: "white", // Change the border color to white
                      },
                    }}
                  />

                  <Box
                    component={"div"}
                    sx={{
                      paddingBottom: 3,
                      paddingLeft: 4,
                      paddingTop: 2,
                      display: "flex",
                      justifyContent: "flex-start",
                      alignItems: "center",
                    }}
                  >
                    <Box component={"a"} href="/login">
                      Olvido la Contraseña?
                    </Box>
                  </Box>
                  <Box
                    component={"div"}
                    sx={{
                      paddingBottom: 2,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Button
                      // backgroundColor="rgb(100, 100, 100)"
                      sx={{
                        width: "85%",
                        fontWeight: "bold",
                        color: "rgb(60, 60, 60)",
                        backgroundColor: "rgb(260, 260, 260)",
                        fontSize: 15,
                        fontWeight: "uppercase",
                        "&:hover": {
                          color: "rgb(200, 200, 200)",
                          backgroundColor: "rgb(60, 60, 60)",
                        },
                      }}
                      onClick={handleLogin}
                    >
                      {loading ? "Conectando" : "Conectar"}
                    </Button>
                  </Box>
                </form>
                <Snackbar
                  open={open}
                  autoHideDuration={5000}
                  onClose={handleClose}
                  message={error}
                />
              </Box>
            </CardContent>
          </Card>
          <Root>
            <Divider sx={{ marginTop: 5, paddingBottom: 3 }}>
              ¿Eres nuevo?
            </Divider>
          </Root>
          <Box
            component={"div"}
            sx={{
              paddingBottom: 2,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Button
              // backgroundColor="rgb(100, 100, 100)"
              sx={{
                width: "80%",
                fontWeight: "bold",
                color: "rgb(60, 60, 60)",
                backgroundColor: "transparent",
                fontSize: 15,
                fontWeight: "uppercase",
                "&:hover": {
                  color: "rgb(200, 200, 200)",
                  backgroundColor: "rgb(60, 60, 60)",
                },
              }}
            >
              <Box component={"a"} href="/register">
                Crear tu cuenta
              </Box>
            </Button>
          </Box>
        </Box>
        <Footer />
      </Box>
    </Fragment>
  );
}

export default LoginComponent;
