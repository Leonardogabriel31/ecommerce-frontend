import React, { useState } from "react";
import EmblaCarousel from "./js/emblaCarousel";
import "./css/base.css";
import "./css/embla.css";
import "./css/sandbox.css";

const OPTIONS = { axis: "y", loop: true };
const images = [
  {
    imgPath: "/ropatotal.jpg",
    href: "/categories/clothesTotal",
  },
  {
    imgPath: "/tecnologia.jpg",
    href: "/categories/tecnology",
  },
  {
    imgPath: "/homeAppliance.jpg",
    href: "/categories/homeAppliances",
  },
  {
    imgPath: "/accesories.jpg",
    href: "/categories/accesories",
  },
];

function CarouselPhotography() {
  
  return (
    <div>
      <EmblaCarousel slides={images} options={OPTIONS} />
    </div>
  );
}

export default CarouselPhotography;
