import React, { useState } from "react";
import Autoplay from "embla-carousel-autoplay";
import useEmblaCarousel from "embla-carousel-react";
import { Box } from "@mui/material";

const EmblaCarousel = (props) => {
  const { slides, options } = props;
  const [isPlayState, setIsPlayState] = useState(true);

  let timer;

  const handleMouseUp = () => {
    setIsPlayState(false);
    timer = setTimeout(() => {
      setIsPlayState(true);
    }, 4000);
  };

  const [emblaRef] = useEmblaCarousel(options, [
    Autoplay({ playOnInit: isPlayState }),
  ]);

  return (
    <Box
      onMouseUp={handleMouseUp}
      component={"section"}
      sx={{
        width: "100%",
        maxHeight: "100%",
        height: {
          xs: "82.3vh",
          sm: "88.3vh",
          md: "88.3vh",
          lg: "88.3vh",
          xl: "88.3vh",
        },
      }}
    >
      <Box
        sx={{
          overflow: "hidden",
          height: {
            xs: "82.3vh",
            sm: "88.3vh",
            md: "88.3vh",
            lg: "88.3vh",
            xl: "88.3vh",
          },
        }}
        ref={emblaRef}
      >
        <Box
          component={"div"}
          className="embla__container"
          sx={{
            height: {
              xs: "82.3vh",
              sm: "88.3vh",
              md: "88.3vh",
              lg: "88.3vh",
              xl: "88.3vh",
            },
          }}
        >
          {slides.map((image, index) => (
            <Box
              className="embla__slide"
              key={index}
              component={"div"}
              // href={image.href}
            >
              <Box
                component="img"
                sx={{
                  display: "block",
                  overflow: "hidden",
                  width: "100%",
                  minWidth: "10%",
                  height: {
                    xs: "82.3vh",
                    sm: "88.3vh",
                    md: "88.3vh",
                    lg: "88.3vh",
                    xl: "88.3vh",
                  },
                  objectFit: "cover",
                }}
                src={image.imgPath}
              />
            </Box>
          ))}
        </Box>
      </Box>
    </Box>
  );
};

export default EmblaCarousel;
