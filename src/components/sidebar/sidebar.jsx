"use client";
import React, { useState } from "react";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import DashboardIcon from "@mui/icons-material/Dashboard";
import GroupIcon from "@mui/icons-material/Group";
import InventoryIcon from "@mui/icons-material/Inventory";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import { logout, useUserData } from "@/lib/features/actions/user/userAction";
import { useRouter } from "next/navigation";
import { useAppDispatch } from "@/lib/features/hooks/hooks";
import { DrawerHeader, Drawer, AppBar } from "@/styles/index";
import AccountCircle from "@mui/icons-material/AccountCircle";
import Link from "next/link";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  Menu,
  MenuItem,
  Typography,
} from "@mui/material";

const dataSidebar = [
  { name: "Dashboard", route: "/admin/dashboard", icon: <DashboardIcon /> },
  {
    name: "Gestion de usuarios",
    route: "/admin/users",
    icon: <GroupIcon />,
  },
  {
    name: "Gestion de productos",
    route: "/admin/products",
    icon: <InventoryIcon />,
  },
];

export default function Sidebar() {
  const theme = useTheme();
  const [open, setOpen] = useState(false);
  const [auth, setAuth] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);
  const router = useRouter();
  const dispatch = useAppDispatch();
  const userData = useUserData();
  let userLogin = null;

  if (userData) {
    userLogin = userData.user;
    // console.log(userLogin);
  }

  const handleLogout = async () => {
    try {
      const response = await dispatch(logout());
      if (response === undefined) {
        router.push("/login");
      }
    } catch (error) {
      console.error("Error logging out:", error);
    }
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleChange = (event) => {
    setAuth(event.target.checked);
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Box>
      <CssBaseline />
      <AppBar
        sx={{
          backgroundColor: "white",
          color: "black",
          boxShadow: "none",
          border: "none",
        }}
        position="fixed"
        open={open}
      >
        <Toolbar
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box component={"div"}>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              sx={{
                marginRight: 5,
                ...(open && { display: "none" }),
              }}
            >
              <MenuIcon />
            </IconButton>
          </Box>
          <IconButton
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
          >
            <AccountCircle sx={{ fontSize: 45 }} />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            keepMounted
            transformOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            open={Boolean(anchorEl)}
            // open={true}
            onClose={handleClose}
            sx={{
              width: 350,
              height: 440,
              mt: 1,
              "& .MuiPaper-root": {
                width: "100%",
                height: "100%",
              },
            }}
          >
            <Box
              component={"div"}
              sx={{
                padding: 3,
                display: "ruby-text",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                // onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle sx={{ fontSize: 70 }} />
              </IconButton>
              <Typography gutterBottom variant="h5" component="div">
                {userLogin?.person.fullName}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                {userLogin?.email}
              </Typography>
            </Box>
            <Divider sx={{ paddingTop: 3 }}></Divider>
            {/* <Box component={"div"} sx={{padding: 2}}> */}
            <Box component={"div"} sx={{ paddingBottom: 1, paddingTop: 1 }}>
              <Button onClick={handleClose} sx={{ color: "gray", width: 318 }}>
                <Link href={"/admin/profile"} passHref>
                  Profile
                </Link>
              </Button>
            </Box>
            <Box component={"div"} sx={{  }}>
              <Button onClick={handleClose} sx={{ color: "gray", width: 318 }}>
                <Link href={"/admin/profile"} passHref>
                  My account
                </Link>
              </Button>
            </Box>
            {/* </Box> */}
          </Menu>
        </Toolbar>
      </AppBar>
      {/* </Box> */}
      <Drawer variant="permanent" open={open}>
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          {dataSidebar.map((text, index) => (
            <ListItem key={index} disablePadding sx={{ display: "block" }}>
              <Link href={text.route} passHref>
                <ListItemButton
                  sx={{
                    minHeight: 48,
                    justifyContent: open ? "initial" : "center",
                    px: 2.5,
                  }}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                    }}
                  >
                    {text.icon}
                  </ListItemIcon>
                  <ListItemText
                    primary={text.name}
                    sx={{ opacity: open ? 1 : 0 }}
                  />
                </ListItemButton>
              </Link>
            </ListItem>
          ))}
        </List>
        <Divider />
        <List>
          <ListItem disablePadding sx={{ display: "block" }}>
            <ListItemButton
              sx={{
                minHeight: 48,
                justifyContent: open ? "initial" : "center",
                px: 2.5,
              }}
            >
              <ListItemIcon
                sx={{
                  minWidth: 0,
                  mr: open ? 3 : "auto",
                  justifyContent: "center",
                }}
              >
                <InboxIcon />
              </ListItemIcon>
              <ListItemText
                primary={"Modo nocturno"}
                sx={{ opacity: open ? 1 : 0 }}
              />
            </ListItemButton>
          </ListItem>
          <ListItem
            disablePadding
            sx={{ display: "block" }}
            onClick={handleLogout}
          >
            <ListItemButton
              sx={{
                minHeight: 48,
                justifyContent: open ? "initial" : "center",
                px: 2.5,
              }}
              onClick={handleLogout}
            >
              <ListItemIcon
                sx={{
                  minWidth: 0,
                  mr: open ? 3 : "auto",
                  justifyContent: "center",
                }}
              >
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary={"salir"} sx={{ opacity: open ? 1 : 0 }} />
            </ListItemButton>
          </ListItem>
        </List>
      </Drawer>
    </Box>
  );
}
