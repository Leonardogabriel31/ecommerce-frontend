"use client";
import React, { useEffect, useState } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import {
  Avatar,
  Button,
  Divider,
  Drawer,
  List,
  ListItem,
  Menu,
  MenuItem,
  Tooltip,
} from "@mui/material";
import Link from "next/link";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PersonIcon from "@mui/icons-material/Person";
import { useAppDispatch, useAppSelector } from "@/lib/features/hooks/hooks";
import { changeColor } from "@/lib/features/reducers/colorReducer";
import { getCartTotal } from "@/lib/features/reducers/products/productsReducer";
import settings from "@/services/buttonsNavbar";
import menuUser from "@/services/menuUser";
import { useRouter } from "next/navigation";
import { logout, useUserData } from "@/lib/features/actions/user/userAction";
import {
  StyledToolbar,
  StyledToolbarButtonsHidden,
  Search,
  SearchIconWrapper,
  StyledInputBase,
} from "@/styles/index";

function Navbar() {
  const [openDrawer, setOpenDrawer] = useState(false);
  const [hovered, setHovered] = useState(false);
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);

  const theme = useTheme();
  const router = useRouter();
  const color = useAppSelector((state) => state.color.color);
  const { cart, totalQuantity } = useAppSelector((state) => state.product);
  const isSmallScreen = useMediaQuery(theme.breakpoints.down("806"));
  const dispatch = useAppDispatch();
  const toggleDrawer = (open) => () => {
    setOpenDrawer(open);
  };

  const userData = useUserData();
  let token = null;
  useEffect(() => {
    dispatch(getCartTotal());
    // console.log(userData)
  }, [cart, dispatch]);

  if (userData) {
    token = userData.token;
  }
  const handleLogout = async () => {
    try {
      const response = await dispatch(logout());
      if (response === undefined) {
        router.push("/login"); // Redirigir al usuario a la pantalla de login
      }
    } catch (error) {
      console.error("Error logging out:", error);
    }
  };

  const handleMouseEnter = () => {
    setHovered(true);
    dispatch(changeColor("black"));
  };

  const handleMouseLeave = () => {
    setHovered(false);
    dispatch(changeColor("white"));
  };

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <Box
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      sx={{ flexGrow: 1 }}
    >
      <AppBar
        position="fixed"
        elevation={0}
        sx={{
          background: hovered ? "white" : "transparent",
          transition: "background 0.4s",
          padding: 2,
          paddingLeft: 4,
        }}
      >
        <Box
          component={"div"}
          sx={{
            display: "flex",
            justifyContent: { sm: "flex-end" },
            alignItems: "center",
          }}
        >
          <Typography
            component="div"
            sx={{
              flexGrow: 1,
              fontSize: { xs: 30, sm: 40, md: 50, lg: 60, xl: 70 },
              display: { xs: "block", sm: "none" },
              color: color,
              transition: "color 0.4s",
            }}
          >
            Hola mundo
          </Typography>
          <StyledToolbarButtonsHidden>
            <Link href="/">
              {isSmallScreen ? (
                <PersonIcon
                  sx={{ color: color, transition: "color 0.4s", fontSize: 35 }}
                />
              ) : (
                ""
              )}{" "}
            </Link>

            <Link href="/">
              {isSmallScreen ? (
                <ShoppingCartIcon
                  sx={{ color: color, transition: "color 0.4s", fontSize: 30 }}
                />
              ) : (
                ""
              )}
            </Link>
          </StyledToolbarButtonsHidden>
        </Box>

        <Toolbar>
          {isSmallScreen && (
            <IconButton
              edge="start"
              aria-label="menu"
              onClick={toggleDrawer(true)}
            >
              <MenuIcon
                sx={{ color: color, transition: "color 0.4s", fontSize: 25 }}
              />
            </IconButton>
          )}
          <Typography
            noWrap
            component="div"
            sx={{
              transition: "color 0.4s",
              color: color,
              flexGrow: 1,
              fontSize: { sm: 40, md: 50, lg: 60, xl: 70 },
              display: { xs: "none", sm: "block" },
            }}
          >
            Hola mundo
          </Typography>
          <Search
            sx={{
              backgroundColor: color,
              transition: "background 0.4s",
              "&:hover": {
                backgroundColor: "rgb(40, 40, 40)",
              },
            }}
          >
            <SearchIconWrapper>
              <SearchIcon
                sx={{
                  transition: "color 0.4s",
                  color: color == "white" ? "black" : "inherit",
                }}
              />
            </SearchIconWrapper>
            <StyledInputBase
              sx={{
                transition: "color 0.4s",
                color: color == "white" ? "black" : "inherit",
              }}
              placeholder="Search…"
              inputProps={{ "aria-label": "search" }}
            />
          </Search>
          <Box component={"div"}>
            <StyledToolbar>
              <Box sx={{ flexGrow: 0 }}>
                <Tooltip title="Open settings">
                  <IconButton onClick={handleOpenUserMenu}>
                    <Avatar
                      sx={{
                        color: color,
                        transition: "color 0.4s",
                        "&:hover": {
                          color: "white",
                          transition: "color 0.4s",
                        },
                      }}
                      alt="Remy Sharp"
                    />
                  </IconButton>
                </Tooltip>
                <Menu
                  sx={{ mt: "45px" }}
                  id="menu-appbar"
                  anchorEl={anchorElUser}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={Boolean(anchorElUser)}
                  // open={true}
                  onClose={handleCloseUserMenu}
                >
                  {menuUser.map((user, index) => (
                    <Box
                      key={index}
                      component={"a"}
                      href={user.href}
                      sx={{ textAlign: "center" }}
                    >
                      <MenuItem onClick={handleCloseUserMenu}>
                        {user.name}
                      </MenuItem>
                    </Box>
                  ))}
                  <Divider sx={{ paddingBottom: 1 }}></Divider>
                  <MenuItem onClick={handleLogout} sx={{ paddingBottom: 1 }}>
                    Salir
                  </MenuItem>
                </Menu>
              </Box>
            </StyledToolbar>
          </Box>
        </Toolbar>
        <Box component={"div"}>
          <StyledToolbar>
            {settings.map((button, index) => (
              <Link key={index} href={button.href}>
                <Button
                  sx={{
                    display: { xs: isSmallScreen ? "none" : "inline" },
                    fontWeight: "bold",
                    color: color,
                    fontSize: 12,
                    // fontWeight: "uppercase",
                    "&:hover": {
                      color: "rgb(200, 200, 200)",
                      backgroundColor: "rgb(60, 60, 60)",
                    },
                  }}
                >
                  {button.name}
                </Button>
              </Link>
            ))}
          </StyledToolbar>
        </Box>
      </AppBar>
      <Drawer anchor="left" open={openDrawer} onClose={toggleDrawer(false)}>
        <List>
          {settings.map((setting, index) => (
            <Link href={setting.href} key={index}>
              <ListItem>{setting.name}</ListItem>
            </Link>
          ))}
        </List>
      </Drawer>
    </Box>
  );
}

export default Navbar;
