"use client";
import React, { Fragment } from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  Divider,
  Snackbar,
  Typography,
} from "@mui/material";
import InputHandler from "@/services/inputs";
import { useAppDispatch, useAppSelector } from "@/lib/features/hooks/hooks";
import {
  createUser,
  resetForm,
  setOpenSnackbar,
} from "../../lib/features/actions/register/registerAction";
import { useRouter } from "next/navigation";
import { REGISTER_CREATE_USER } from "../../lib/features/types/register/registerTypes";

function AdminRegisterComponent({ onClose }) {
  const state = useAppSelector((state) => state.register);
  const error = state.error;
  const open = state.openSnackbar;
  const successMessage = state.successMessage;
  const dispatch = useAppDispatch();
  const router = useRouter();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const form = {
      email: state.form.email,
      password: state.form.password,
      person: {
        fullName: state.form.fullName,
        country: state.form.country,
        phoneNumber: state.form.phoneNumber,
        codePostal: state.form.codepostal,
      },
    };
    try {
      const response = await dispatch(createUser(form));
      if (response.type === REGISTER_CREATE_USER) {
        dispatch(resetForm());
        dispatch(setOpenSnackbar(true));
        onClose();
      }
    } catch (error) {
      if (error) {
        dispatch(setOpenSnackbar(true));
      }
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    dispatch(setOpenSnackbar(false));
  };

  return (
    <Fragment>
      <Box
        component={"div"}
        sx={{
          display: "grid",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
          marginBottom: "3.7vh",
        }}
      >
        <Typography
          component="div"
          sx={{
            flexGrow: 1,
            fontSize: 26,
            color: "black",
            paddingTop: 4,
            paddingRight: 20,
          }}
        >
          Registrar usuario
        </Typography>
        <CardContent
          sx={{
            textAligne: "justify",
            display: "flow",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Box
            component="div"
            sx={{
              display: "flow",
              justifyContent: "center",
              alignItems: "center",
              width: 380,
            }}
          >
            <form>
              <InputHandler />
              <Box
                component={"div"}
                sx={{
                  paddingBottom: 2,
                  paddingTop: 2,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Button
                  type="submit"
                  backgroundColor="rgb(100, 100, 100)"
                  sx={{
                    width: "85%",
                    fontWeight: "bold",
                    color: "rgb(60, 60, 60)",
                    backgroundColor: "rgb(260, 260, 260)",
                    fontSize: 15,
                    fontWeight: "uppercase",
                    "&:hover": {
                      color: "rgb(200, 200, 200)",
                      backgroundColor: "rgb(60, 60, 60)",
                    },
                  }}
                  onClick={handleSubmit}
                >
                  Continuar
                </Button>
              </Box>
            </form>
            <Snackbar
              open={open}
              autoHideDuration={5000}
              onClose={handleClose}
              message={error ? error.msg : successMessage}
            />
          </Box>
        </CardContent>
      </Box>
    </Fragment>
  );
}

export default AdminRegisterComponent;
