"use client";
import React, { Fragment, useEffect, useState } from "react";
import { Box } from "@mui/material";
import CarouselPhotography from "@/components/carruselPhotography/carouselPhotography";
import Sidebar from "../sidebar/sidebar";

function ContainerHome() {

  return (
    <Fragment>
      <Box
        sx={{
          wordWrap: "break-word",
          overflowWrap: "break-word",
          width: "100%",
          flexGrow: 1,
        }}
      >
        <CarouselPhotography />
      </Box>
    </Fragment>
  );
}

export default ContainerHome;
