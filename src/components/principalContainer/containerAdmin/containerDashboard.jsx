"use client";
import React from "react";
import Sidebar from "@/components/sidebar/sidebar";
import Dashboard from "@/components/principalContainer/containerAdmin/componentsContainerAdmin/dashboard/dashboard";
import { Box } from "@mui/material";

function ContainerDashboard() {
  return (
    <Box sx={{ display: "flex", backgroundColor: "#dedede" }}>
      <Sidebar />
      <Dashboard />
    </Box>
  );
}

export default ContainerDashboard;
