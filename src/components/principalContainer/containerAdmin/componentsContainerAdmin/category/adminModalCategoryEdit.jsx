"use client";
import React, { Fragment, useEffect } from "react";
import {
  Button,
  TextField,
  Dialog,
  DialogContent,
  Box,
  Typography,
  CardContent,
  Stack,
} from "@mui/material";
import { updateEditingCategoryForm } from "@/lib/features/actions/category/categoryAction";
import { useAppDispatch, useAppSelector } from "@/lib/features/hooks/hooks";

function AdminModalCategoryEdit({ open, onClose, category, onSave }) {
  const editingCategoryForm = useAppSelector((state) => state.category.editingCategoryForm);
  const dispatch = useAppDispatch();

  const handleSave = () => {
    onSave({ ...category, ...editingCategoryForm });
  };

  const handleNameChange = (e) => {
    dispatch(updateEditingCategoryForm({ name: e.target.value }));
    dispatch(updateEditingCategoryForm({ ...editingCategoryForm, name: e.target.value }));
  };

  return (
    <Fragment>
      <Dialog
        open={open}
        onClose={onClose}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <DialogContent sx={{ width: 500, maxHeight: 900 }}>
          <Box
            component={"div"}
            sx={{
              display: "grid",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
              marginBottom: "3.7vh",
            }}
          >
            <Typography
              component="div"
              sx={{
                flexGrow: 1,
                fontSize: 26,
                color: "black",
                paddingTop: 4,
                paddingRight: 20,
              }}
            >
              Editar categoria
            </Typography>
            <CardContent
              sx={{
                textAligne: "justify",
                display: "flow",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Box
                component="div"
                sx={{
                  display: "flow",
                  justifyContent: "center",
                  alignItems: "center",
                  width: 380,
                }}
              >
                <Stack spacing={2} margin={2}>
                  <TextField
                    required
                    variant="outlined"
                    label="Nombre del producto"
                    type="text"
                    placeholder="Nombre"
                    name="name"
                    value={editingCategoryForm.name}
                    onChange={handleNameChange}
                    // error={error}
                    // helperText={error ? helperText : ""}
                    autoComplete="off"
                  />
                </Stack>
                <Box
                  component={"div"}
                  sx={{
                    paddingBottom: 2,
                    paddingTop: 2,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Button
                    type="submit"
                    backgroundColor="rgb(100, 100, 100)"
                    sx={{
                      width: "85%",
                      fontWeight: "bold",
                      color: "rgb(60, 60, 60)",
                      backgroundColor: "rgb(260, 260, 260)",
                      fontSize: 15,
                      fontWeight: "uppercase",
                      "&:hover": {
                        color: "rgb(200, 200, 200)",
                        backgroundColor: "rgb(60, 60, 60)",
                      },
                    }}
                    onClick={handleSave}
                  >
                    Continuar
                  </Button>
                </Box>
              </Box>
            </CardContent>
          </Box>
        </DialogContent>
      </Dialog>
    </Fragment>
  );
}

export default AdminModalCategoryEdit;
