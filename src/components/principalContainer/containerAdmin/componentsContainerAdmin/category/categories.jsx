import React, { Fragment } from "react";
import TableCategory from "@/components/principalContainer/containerAdmin/componentsContainerAdmin/category/tableCategory";
import { Box, Button, Grid } from "@mui/material";
import AdminModalCategories from "./adminModalCategory";

function Categories() {
  return (
    <Fragment>
      <Box
        component={"div"}
        sx={{
          marginTop: 30,
        }}
      >
        <Box
          component={"div"}
          sx={{
            paddingBottom: 4,
          }}
        >
          <AdminModalCategories />
        </Box>
        <TableCategory />
      </Box>
    </Fragment>
  );
}

export default Categories;
