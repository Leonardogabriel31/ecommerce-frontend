import React, { Fragment, useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import {
  Box,
  CardContent,
  IconButton,
  Snackbar,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useAppDispatch, useAppSelector } from "@/lib/features/hooks/hooks";
import { createCategory, setForm, getCategories } from "@/lib/features/actions/category/categoryAction";

export default function AdminModalCategories() {
  const [open, setOpen] = useState(false);
  const form = useAppSelector((state) => state.category.form);
  const state = useAppSelector((state) => state.category);
  const error = state.error;
  const dispatch = useAppDispatch();
  // const router = useRouter();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const form = {
      name: state.form.name,
    };
    await dispatch(createCategory(form));
    await dispatch(getCategories()).then(() => {
      setOpen(false);
      dispatch(setForm({}));
    });
  };

  const handleChangeName = (e) => {
    dispatch(setForm({ ...form, name: e.target.value }));
  };

  return (
    <Fragment>
      <Button
        variant="outlined"
        onClick={handleClickOpen}
        sx={{
          width: "25%",
          fontWeight: "bold",
          color: "rgb(60, 60, 60)",
          backgroundColor: "transparent",
          fontSize: 15,
          fontWeight: "uppercase",
          "&:hover": {
            color: "rgb(200, 200, 200)",
            backgroundColor: "rgb(60, 60, 60)",
          },
        }}
      >
        Registrar categoria
      </Button>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        BackdropProps={{
          onClick: () => setOpen(false),
        }}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <DialogContent sx={{ width: 500, maxHeight: 900 }}>
          <Box
            component={"div"}
            sx={{
              display: "grid",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
              marginBottom: "3.7vh",
            }}
          >
            <Typography
              component="div"
              sx={{
                flexGrow: 1,
                fontSize: 26,
                color: "black",
                paddingTop: 4,
                paddingRight: 20,
              }}
            >
              Registrar producto
            </Typography>
            <CardContent
              sx={{
                textAligne: "justify",
                display: "flow",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Box
                component="div"
                sx={{
                  display: "flow",
                  justifyContent: "center",
                  alignItems: "center",
                  width: 380,
                }}
              >
                <form>
                  <Stack spacing={2} margin={2}>
                    <TextField
                      required
                      variant="outlined"
                      label="Nombre del producto"
                      type="text"
                      placeholder="Nombre"
                      name="name"
                      value={form.name}
                      onChange={handleChangeName}
                      // error={error}
                      // helperText={error ? helperText : ""}
                      autoComplete="off"
                    />
                  </Stack>
                  <Box
                    component={"div"}
                    sx={{
                      paddingBottom: 2,
                      paddingTop: 2,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Button
                      type="submit"
                      backgroundColor="rgb(100, 100, 100)"
                      sx={{
                        width: "85%",
                        fontWeight: "bold",
                        color: "rgb(60, 60, 60)",
                        backgroundColor: "rgb(260, 260, 260)",
                        fontSize: 15,
                        fontWeight: "uppercase",
                        "&:hover": {
                          color: "rgb(200, 200, 200)",
                          backgroundColor: "rgb(60, 60, 60)",
                        },
                      }}
                      onClick={handleSubmit}
                    >
                      Continuar
                    </Button>
                  </Box>
                </form>
                {/* <Snackbar
                  open={open}
                  autoHideDuration={5000}
                  onClose={handleClose}
                  message={error ? error.msg : successMessage}
                /> */}
              </Box>
            </CardContent>
          </Box>
        </DialogContent>
      </Dialog>
    </Fragment>
  );
}
