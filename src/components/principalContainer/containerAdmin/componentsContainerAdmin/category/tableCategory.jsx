import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import { useAppDispatch, useAppSelector } from "@/lib/features/hooks/hooks";
import {
  getCategories,
  updateCategory,
  deleteCategory,
  setEditingCategory,
  updateEditingCategoryForm,
} from "@/lib/features/actions/category/categoryAction";
import { TableHead } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import SaveIcon from "@mui/icons-material/Save";
import AdminModalCategoryEdit from "@/components/principalContainer/containerAdmin/componentsContainerAdmin/category/adminModalCategoryEdit";

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

export default function TableCategory() {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [openModal, setOpenModal] = useState(false);
  const categories = useAppSelector((state) => state.category.categoriesAll);
  const editingCategory = useAppSelector(
    (state) => state.category.editingCategory
  );
  const dispatch = useAppDispatch();
  useEffect(() => {
    if (getCategories) {
      dispatch(getCategories());
    }
  }, [dispatch]);

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - categories.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleEditCategory = (category) => {
    dispatch(setEditingCategory(category));
    setOpenModal(true);
  };

  const handleSaveEditedCategory = async (updatedCategory) => {
    await dispatch(updateCategory(updatedCategory._id, updatedCategory));
    await dispatch(getCategories()).then(() => {
      dispatch(setEditingCategory(null));
      setOpenModal(false);
      dispatch(updateEditingCategoryForm({}));
    });
  };

  return (
    <TableContainer component={Paper}>
      <Table sx={{ width: 900 }} aria-label="custom pagination table">
        <TableHead>
          <TableRow style={{ backgroundColor: "#546E7A" }}>
            <TableCell
              style={{ width: 160, fontWeight: "bold", color: "white" }}
              align="left"
              scope="row"
            >
              Category Name
            </TableCell>
            <TableCell
              style={{ width: 160, fontWeight: "bold", color: "white" }}
              align="center"
            >
              Actions
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? categories.slice(
                page * rowsPerPage,
                page * rowsPerPage + rowsPerPage
              )
            : categories
          ).map((category, index) => (
            <TableRow key={index}>
              <TableCell style={{ width: 160 }} component="th" scope="row">
                {category.name}
              </TableCell>

              <TableCell
                style={{
                  width: 160,
                }}
                align="center"
              >
                <IconButton>
                  <EditIcon onClick={() => handleEditCategory(category)} />
                </IconButton>
                <IconButton
                  onClick={() => {
                    dispatch(deleteCategory(category._id));
                  }}
                >
                  <DeleteIcon />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
              colSpan={3}
              count={categories.length}
              rowsPerPage={rowsPerPage}
              page={page}
              slotProps={{
                select: {
                  inputProps: {
                    "aria-label": "rows per page",
                  },
                  native: true,
                },
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
      <AdminModalCategoryEdit
        open={openModal}
        onClose={() => setOpenModal(false)}
        category={editingCategory}
        onSave={handleSaveEditedCategory}
      />
    </TableContainer>
  );
}
