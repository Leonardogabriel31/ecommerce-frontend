"use client";
import React from "react";
import { Box, Grid } from "@mui/material";
import MonetizationOnIcon from "@mui/icons-material/MonetizationOn";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import LocalMallIcon from "@mui/icons-material/LocalMall";
import ChartsDashboard from "@/components/principalContainer/containerAdmin/componentsContainerAdmin/dashboard/chartsDashboard";
import PieChartsDashboard from "@/components/principalContainer/containerAdmin/componentsContainerAdmin/dashboard/pieChartsDashboard";
import TableDashboard from "@/components/principalContainer/containerAdmin/componentsContainerAdmin/dashboard/tableDashboard";

function Dashboard() {
  return (
    <Box
      component="main"
      sx={{ flexGrow: 1, padding: 3, marginTop: 7, paddingBottom: 7 }}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} md={4} lg={4} xl={4}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center",
              backgroundColor: "white",
              padding: 3,
              paddingTop: 5,
              paddingBottom: 5,
              fontSize: 18,
              borderRadius: 3,
              gap: 1,
            }}
          >
            <MonetizationOnIcon sx={{ fontSize: 50, color: "#FFA50C" }} />
            <Box component={"div"}>Total Sales</Box>
          </Box>
        </Grid>

        <Grid item xs={12} sm={6} md={4} lg={4} xl={4}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center",
              backgroundColor: "white",
              padding: 3,
              paddingTop: 5,
              paddingBottom: 5,
              fontSize: 18,
              borderRadius: 3,
              gap: 1,
            }}
          >
            <ShoppingCartIcon sx={{ fontSize: 50, color: "green" }} />
            <Box component={"div"}>Total Orders</Box>
          </Box>
        </Grid>

        <Grid item xs={12} sm={6} md={4} lg={4} xl={4}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center",
              backgroundColor: "white",
              padding: 3,
              paddingTop: 5,
              paddingBottom: 5,
              fontSize: 18,
              borderRadius: 3,
              gap: 1,
            }}
          >
            <LocalMallIcon sx={{ fontSize: 50, color: "blue" }} />
            <Box component={"div"}>Total Products</Box>
          </Box>
        </Grid>

        <Grid item xs={12} sm={12} md={8} lg={8} xl={8}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "white",
              paddingTop: 4,
              paddingRight: 4,
              paddingBottom: 4,
              fontSize: 12,
              borderRadius: 3,
              marginBottom: 2,
              width: "100%", 
              height: "100%",
            }}
          >
            <ChartsDashboard />
          </Box>
        </Grid>

        <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "white",
              fontSize: 15,
              borderRadius: 3,
              marginBottom: 2,
              paddingBottom: 4,
              paddingTop: 4,
              width: "100%", 
              height: "100%", 
            }}
          >
            <PieChartsDashboard />
          </Box>
        </Grid>

        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "white",
              paddingTop: 5,
              paddingBottom: 5,
              paddingRight: 5,
              paddingLeft: 5,
              fontSize: 12,
              borderRadius: 3,
              marginBottom: 2,
            }}
          >
            <TableDashboard />
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
}

export default Dashboard;
