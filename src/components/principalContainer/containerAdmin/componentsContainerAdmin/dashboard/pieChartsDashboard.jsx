import React, { Fragment } from "react";
import {
  Legend,
  Pie,
  PieChart,
  ResponsiveContainer,
  Tooltip,
} from "recharts";

const data = [
  { name: "Social media", weight: 60 },
  { name: "Afiliate visitors", weight: 70 },
  { name: "Purshased visitors", weight: 65 },
  { name: "By advertisement", weight: 85 },
];

function PieChartsDashboard() {
  return (
    <Fragment>
      <ResponsiveContainer
        style={{
          backgroundColor: "white",
          borderRadius: 15,
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
        aspect={4 / 3}
      >
        <PieChart width={500} height={300}>
          <Pie
            data={data}
            dataKey={"weight"}
            cx={"50%"}
            cy={"50%"}
            outerRadius={"100%"}
            fill="#6b48ff"
          />
          <Tooltip />
        </PieChart>
      </ResponsiveContainer>
    </Fragment>
  );
}

export default PieChartsDashboard;
