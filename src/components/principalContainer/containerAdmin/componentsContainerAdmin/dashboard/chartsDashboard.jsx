import React, { Fragment } from "react";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

const data = [
  { name: "Ene", age: 10, weight: 60 },
  { name: "Feb", age: 25, weight: 70 },
  { name: "Mar", age: 15, weight: 65 },
  { name: "Abr", age: 35, weight: 85 },
  { name: "May", age: 12, weight: 48 },
  { name: "Jun", age: 30, weight: 69 },
  { name: "Jul", age: 15, weight: 78 },
  { name: "Agst", age: 10, weight: 60 },
  { name: "Sept", age: 25, weight: 70 },
  { name: "Oct", age: 15, weight: 65 },
  { name: "Nov", age: 35, weight: 85 },
  { name: "Dic", age: 12, weight: 48 },
];

function ChartsDashboard() {
  return (
    <Fragment>
      <ResponsiveContainer
        style={{
          backgroundColor: "white",
          borderRadius: 15,
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
        aspect={3}
      >
        <BarChart data={data} width={500} height={300}>
          <CartesianGrid strokeDasharray={"4 1 2"} />
          <XAxis dataKey={"name"} />
          <YAxis />
          <Tooltip />
          <Bar dataKey={"weight"} fill="#6b48ff" />
          <Bar dataKey={"age"} fill="#1ee3cf" />
        </BarChart>
      </ResponsiveContainer>
    </Fragment>
  );
}

export default ChartsDashboard;
