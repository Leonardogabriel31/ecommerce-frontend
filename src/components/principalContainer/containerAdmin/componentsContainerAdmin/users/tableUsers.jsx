import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import { useAppDispatch, useAppSelector } from "@/lib/features/hooks/hooks";
import {
  deleteUsers,
  getUsers,
  setEditingUser,
  updateEditingUserForm,
  updateUser,
} from "@/lib/features/actions/register/registerAction";
import AdminModalUserEdit from "@/components/principalContainer/containerAdmin/componentsContainerAdmin/users/adminModalUserEdit";
import { TableHead } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

export default function Tableusers() {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const users = useAppSelector((state) => state.register.data);
  const [openModal, setOpenModal] = useState(false);
  const editingUser = useAppSelector((state) => state.register.editingUser);
  const dispatch = useAppDispatch();
  useEffect(() => {
    if (getUsers) {
      dispatch(getUsers());
    }
  }, [dispatch]);

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - users.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleEditUser = (user) => {
    dispatch(setEditingUser(user));
    setOpenModal(true);
  };

  const handleSaveEditedUser = async (updatedUser) => {
    try {
      await dispatch(updateUser(updatedUser));
      await dispatch(getUsers());
      dispatch(setEditingUser(null));
      setOpenModal(false);
      dispatch(updateEditingUserForm({}));
    } catch (error) {
      console.error("Error saving edited user");
      // alert("Error saving edited user: " + error.message);
    }
  };

  return (
    <TableContainer component={Paper}>
      <Table sx={{ width: 900 }} aria-label="custom pagination table">
        <TableHead>
          <TableRow style={{ backgroundColor: "#546E7A" }}>
            <TableCell
              style={{ width: 160, fontWeight: "bold", color: "white" }}
              align="left"
              scope="row"
            >
              Full Name
            </TableCell>
            <TableCell
              style={{ width: 160, fontWeight: "bold", color: "white" }}
              align="center"
            >
              Email
            </TableCell>
            <TableCell
              style={{ width: 160, fontWeight: "bold", color: "white" }}
              align="center"
            >
              Role
            </TableCell>
            <TableCell
              style={{ width: 160, fontWeight: "bold", color: "white" }}
              align="center"
            >
              Phone Number
            </TableCell>
            <TableCell
              style={{ width: 160, fontWeight: "bold", color: "white" }}
              align="center"
            >
              Actions
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? users.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : users
          ).map((user, index) => (
            <TableRow key={index}>
              <TableCell style={{ width: 160 }} component="th" scope="row">
              {user.person?.fullName}
              </TableCell>
              <TableCell style={{ width: 160 }} align="center">
                {user?.email}
              </TableCell>
              <TableCell style={{ width: 160 }} align="center">
                {user?.role}
              </TableCell>
              <TableCell style={{ width: 160 }} align="center">
                {user.person?.phoneNumber}
              </TableCell>
              <TableCell style={{ width: 160 }} align="center">
                {/* <IconButton>
                  <EditIcon 
                  onClick={() =>
                   console.log(user)
                    // () => handleEditUser(user)
                  }/>
                </IconButton> */}
                <IconButton
                  onClick={() => {
                    // console.log(user.person.fullName)
                    dispatch(deleteUsers(user._id));
                  }}
                >
                  <DeleteIcon />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
              colSpan={3}
              count={users.length}
              rowsPerPage={rowsPerPage}
              page={page}
              slotProps={{
                select: {
                  inputProps: {
                    "aria-label": "rows per page",
                  },
                  native: true,
                },
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
      <AdminModalUserEdit
        open={openModal}
        onClose={() => setOpenModal(false)}
        user={editingUser}
        onSave={handleSaveEditedUser}
      />
    </TableContainer>
  );
}
