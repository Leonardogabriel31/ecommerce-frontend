import React, { Fragment, useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import AdminRegisterComponent from "../../../../register/adminRegisterComponent";

export default function AdminModalRegister() {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Fragment>
      <Button
        variant="outlined"
        onClick={handleClickOpen}
        sx={{
          width: "25%",
          fontWeight: "bold",
          color: "rgb(60, 60, 60)",
          backgroundColor: "transparent",
          fontSize: 15,
          fontWeight: "uppercase",
          "&:hover": {
            color: "rgb(200, 200, 200)",
            backgroundColor: "rgb(60, 60, 60)",
          },
        }}
      >
        Registrar usuario
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <DialogContent sx={{ width: 500, maxHeight: 900 }}>
          <AdminRegisterComponent onClose={handleClose} />
        </DialogContent>
      </Dialog>
    </Fragment>
  );
}
