"use client";
import Sidebar from "@/components/sidebar/sidebar";
import { useUserData } from "@/lib/features/actions/user/userAction";
import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Typography,
  Button,
  IconButton,
} from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import {
  useAppDispatch,
  useAppSelector,
} from "../../../../../../lib/features/hooks/hooks";
import AdminModalUserEdit from "../adminModalUserEdit";
import EditIcon from "@mui/icons-material/Edit";
import {
  getUsers,
  setEditingUser,
  updateEditingUserForm,
  updateUser,
} from "@/lib/features/actions/register/registerAction";

function ProfileComponent() {
  const userLogin = useAppSelector((state) => state.user.user);
  const [openModal, setOpenModal] = useState(false);
  const editingUser = useAppSelector((state) => state.register.editingUser);
  const userData = useUserData();
  const dispatch = useAppDispatch();
  let token = null;
  let userLoginLocalStorage = null;
  let dataUserLogin = null;
  if (userData) {
    // token = userData.token;
    userLoginLocalStorage = userData?.user;
    dataUserLogin = userData;
    console.log(dataUserLogin)
  }
  useEffect(() => {
    if (getUsers) {
      dispatch(getUsers());
    }
  }, [dispatch]);

  const handleSaveEditedUser = async (updatedUser) => {
    try {
      await dispatch(updateUser(updatedUser));
      await dispatch(getUsers());
      dispatch(setEditingUser(null));
      setOpenModal(false);
      dispatch(updateEditingUserForm({}));
    } catch (error) {
      console.error("Error saving edited user");
    }
  };

  const handleEditUser = (userLoginLocalStorage) => {
    dispatch(setEditingUser(userLoginLocalStorage));
    setOpenModal(true);
  };

  return (
    <Fragment>
      <Box
        sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}
      >
        <Sidebar />
        <Box component={"div"} sx={{ marginTop: 30 }}>
          <Grid container spacing={2} sx={{}}>
            <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
              <Card sx={{ minWidth: 500 }}>
                {/* <CardMedia
                  sx={{ height: 140 }}
                  image="/static/images/cards/contemplative-reptile.jpg"
                  title="green iguana"
                /> */}
                <CardContent>
                  <Box
                    component={"div"}
                    sx={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      padding: 2,
                      paddingBottom: 3,
                    }}
                  >
                    <Typography gutterBottom variant="h4" component="div">
                      {userLoginLocalStorage?.person?.fullName}
                    </Typography>
                  </Box>
                  <Box
                    component={"div"}
                    sx={{ padding: 2, paddingLeft: 3, paddingRight: 3 }}
                  >
                    <Box component={"div"}>
                      <Typography variant="h6" color="text.secondary">
                        {userLoginLocalStorage?.email}
                      </Typography>
                    </Box>
                    <Box component={"div"} sx={{ paddingTop: 2 }}>
                      <Typography variant="h6" color="text.secondary">
                        {userLoginLocalStorage?.role}
                      </Typography>
                    </Box>
                    <Box component={"div"} sx={{ paddingTop: 2 }}>
                      <Typography variant="h6" color="text.secondary">
                        {userLoginLocalStorage?.createdAt}
                      </Typography>
                    </Box>
                  </Box>
                </CardContent>
                <Box
                  component={"div"}
                  sx={{
                    padding: 2,
                    paddingLeft: 3,
                    paddingRight: 3,
                    paddingBottom: 3,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <CardActions>
                    {/* <Button size="small">Editar perfil</Button> */}
                    <IconButton
                      onClick={() => handleEditUser(userLoginLocalStorage)}
                    >
                      Editar usuario
                    </IconButton>
                  </CardActions>
                </Box>
              </Card>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <AdminModalUserEdit
        open={openModal}
        onClose={() => setOpenModal(false)}
        user={editingUser}
        onSave={handleSaveEditedUser}
      />
    </Fragment>
  );
}

export default ProfileComponent;
