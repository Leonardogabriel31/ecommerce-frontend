import React, { Fragment } from "react";
import TableUsers from "@/components/principalContainer/containerAdmin/componentsContainerAdmin/users/tableUsers";
import { Box, Button, Grid } from "@mui/material";
import AdminModalRegister from "./adminModalRegister";

function Users() {
  return (
    <Fragment>
      <Box
        component={"div"}
        sx={{
          marginTop: 30,
        }}
      >
        <Box
          component={"div"}
          sx={{
            paddingBottom: 4,
          }}
        >
          <AdminModalRegister />
        </Box>
        <TableUsers />
      </Box>
    </Fragment>
  );
}

export default Users;
