"use client";
import React, { Fragment, useEffect } from "react";
import {
  Button,
  TextField,
  Dialog,
  DialogContent,
  Box,
  Typography,
  CardContent,
  Stack,
} from "@mui/material";
import { updateEditingUserForm } from "@/lib/features/actions/register/registerAction";
import { useAppDispatch, useAppSelector } from "@/lib/features/hooks/hooks";

function AdminModalUserEdit({ open, onClose, user, onSave }) {
  const editingUserForm = useAppSelector(
    (state) => state.register.editingUserForm
  );
  const dispatch = useAppDispatch();
  useEffect(() => {
    if (open && user) {
      dispatch(
        updateEditingUserForm({
          email: user.email,
          person: {
            fullName: user.person?.fullName,
            country: user.person?.country,
            phoneNumber: user.person?.phoneNumber,
            codePostal: user.person?.codePostal,
          },
        })
      );
    }
  }, [open, user, dispatch]);

  const handleSave = () => {
    if (user) {
      onSave({
        ...user,
        person: { ...user.person, ...editingUserForm.person },
      });
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "email") {
      dispatch(updateEditingUserForm({ [email]: value }));
    } else {
      dispatch(
        updateEditingUserForm({
          person: { ...editingUserForm.person, [name]: value },
        })
      );
    }
  };

  return (
    <Fragment>
      <Dialog
        open={open}
        onClose={onClose}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <DialogContent sx={{ width: 500, maxHeight: 900 }}>
          <Box
            component={"div"}
            sx={{
              display: "grid",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
              marginBottom: "3.7vh",
            }}
          >
            <Typography
              component="div"
              sx={{
                flexGrow: 1,
                fontSize: 26,
                color: "black",
                paddingTop: 4,
                paddingRight: 20,
              }}
            >
              Editar usuario
            </Typography>
            <CardContent
              sx={{
                textAligne: "justify",
                display: "flow",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Box
                component="div"
                sx={{
                  display: "flow",
                  justifyContent: "center",
                  alignItems: "center",
                  width: 380,
                }}
              >
                <Stack spacing={2} margin={2}>
                  <TextField
                    required
                    variant="outlined"
                    label="Nombre y Apellido"
                    type="text"
                    placeholder="Nombre y apellido"
                    name="fullName"
                    value={editingUserForm.person?.fullName}
                    onChange={handleChange}
                    // error={error}
                    // helperText={error ? helperText : ""}
                  />
                  <TextField
                    required
                    variant="outlined"
                    label="Correo Electronico"
                    type="email"
                    placeholder="Email"
                    name="email"
                    value={editingUserForm.email}
                    onChange={handleChange}
                    // error={errorEmail}
                    // helperText={error ? helperTextEmail : ""}
                  />
                  <TextField
                    required
                    variant="outlined"
                    label="Telefono"
                    type="text"
                    placeholder="telefono"
                    name="phoneNumber"
                    value={editingUserForm.person?.phoneNumber}
                    onChange={handleChange}
                    // error={errorPhone}
                    // helperText={errorPhone ? helperTextPhone : ""}
                  />

                  <TextField
                    required
                    variant="outlined"
                    label="Pais"
                    type="text"
                    placeholder="Pais"
                    name="country"
                    value={editingUserForm.person?.country}
                    onChange={handleChange}
                    // error={errorCountry}
                    // helperText={errorCountry ? helperTextCountry : ""}
                  />
                  <TextField
                    required
                    variant="outlined"
                    label="Código postal"
                    type="number"
                    placeholder="Código postal"
                    name="codePostal"
                    value={editingUserForm.person?.codePostal}
                    onChange={handleChange}
                    // error={errorCodepostal}
                    // helperText={errorCodepostal ? helperTextCodepostal : ""}
                  />
                </Stack>
                <Box
                  component={"div"}
                  sx={{
                    paddingBottom: 2,
                    paddingTop: 2,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Button
                    type="submit"
                    backgroundColor="rgb(100, 100, 100)"
                    sx={{
                      width: "85%",
                      fontWeight: "bold",
                      color: "rgb(60, 60, 60)",
                      backgroundColor: "rgb(260, 260, 260)",
                      fontSize: 15,
                      fontWeight: "uppercase",
                      "&:hover": {
                        color: "rgb(200, 200, 200)",
                        backgroundColor: "rgb(60, 60, 60)",
                      },
                    }}
                    onClick={handleSave}
                  >
                    Continuar
                  </Button>
                </Box>
              </Box>
            </CardContent>
          </Box>
        </DialogContent>
      </Dialog>
    </Fragment>
  );
}

export default AdminModalUserEdit;
