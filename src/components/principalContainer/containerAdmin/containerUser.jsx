"use client";
import React from "react";
import Sidebar from "@/components/sidebar/sidebar";
import { Box, Button, Grid } from "@mui/material";
import Users from "./componentsContainerAdmin/users/users";

function ContainerUser() {
  return (
    <Box
      sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}
    >
      <Sidebar />
      <Box component={"div"}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
            <Box component={"div"}>
              <Users />
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}

export default ContainerUser;
