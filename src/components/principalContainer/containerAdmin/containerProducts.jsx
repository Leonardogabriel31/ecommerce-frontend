"use client";
import React from "react";
import Sidebar from "@/components/sidebar/sidebar";
import { Box, Button, Grid } from "@mui/material";
import Categories from "./componentsContainerAdmin/category/categories";

function ContainerCategories() {
  return (
    <Box
      sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}
    >
      <Sidebar />
      <Box component={"div"}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
            <Box component={"div"}>
              <Categories />
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}

export default ContainerCategories;
