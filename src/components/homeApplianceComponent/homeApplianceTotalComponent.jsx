"use client";
import React, { Fragment, forwardRef, useEffect, useState } from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  Dialog,
  DialogActions,
  DialogTitle,
  Grid,
  Typography,
  Slide,
} from "@mui/material";
import { useAppDispatch, useAppSelector } from "@/lib/features/hooks/hooks";
import { addToCart } from "../../lib/features/reducers/products/productsReducer";
import Image from "next/image";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import CartBuyComponent from "../cartBuyComponent/cartBuyComponent";

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function HomeApplianceTotalComponent() {
  const items = useAppSelector((state) => state.product.home);
  const dispatch = useAppDispatch();
  const [open, setOpen] = useState(false);

  const handleClick = () => () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Fragment>
      <Box
        component={"div"}
        sx={{
          position: "fixed",
          bottom: 140,
          right: 80,
        }}
      >
        <CartBuyComponent close={handleClose} />
      </Box>
      <Dialog
        TransitionComponent={Transition}
        fullWidth={true}
        maxWidth={"xs"}
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}
      >
        <DialogTitle id="alert-dialog-title">
          <Box
            component={"div"}
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              gap: 2,
              paddingRight: 2,
            }}
          >
            <Box
              component={"img"}
              src="/carrito-de-compras.gif"
              alt="carrito"
              sx={{
                width: 100,
              }}
            />
            <Box component={"div"}>¡Producto añadido al carrito!</Box>
          </Box>
        </DialogTitle>
        <DialogActions sx={{ padding: 2 }}>
          <CartBuyComponent close={handleClose} />
          <Button
            sx={{
              display: "flex",
              fontWeight: "bold",
              color: "rgb(60, 60, 60)",
              fontSize: 15,
              fontWeight: "uppercase",
              gap: 1,
              "&:hover": {
                color: "rgb(200, 200, 200)",
                backgroundColor: "rgb(60, 60, 60)",
              },
            }}
            onClick={handleClose}
            autoFocus
          >
            Cerrar
          </Button>
        </DialogActions>
      </Dialog>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        padding="25px"
        marginTop="250px"
        gap={4}
        spacing={2}
        columns={{ lg: 1, md: 1 }}
      >
        {items.map((product, index) => (
          <Box component={"div"} key={index}>
            <Grid
              item
              xs={3}
              sx={{
                display: "flex",
                gap: 4,
              }}
            >
              <Box variant="div">
                <Card
                  key={product.id}
                  sx={{
                    width: 360,
                    height: 500,
                  }}
                >
                  <Box
                    component={"div"}
                    sx={{ display: "flex", justifyContent: "center" }}
                  >
                    <Image
                      height={100}
                      width={360}
                      style={{
                        minHeight: 300,
                        maxHeight: 300,
                        maxWidth: "100%",
                        objectFit: "inherit",
                      }}
                      src={product.img}
                      alt={product.title}
                    />
                  </Box>
                  <CardContent sx={{ textAligne: "justify" }}>
                    <Typography
                      variant="h5"
                      component="div"
                      sx={{
                        padding: 2,
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                      }}
                    >
                      {product.title}
                    </Typography>
                    <Typography
                      component="div"
                      sx={{
                        fontSize: 15,
                        paddingLeft: 2,
                        paddingBottom: 2,
                      }}
                    >
                      {product.price}$
                    </Typography>

                    <Box component={"div"} sx={{ paddingBottom: 3 }}>
                      <Button
                        sx={{
                          display: "flex",
                          fontWeight: "bold",
                          color: "rgb(60, 60, 60)",
                          fontSize: 15,
                          fontWeight: "uppercase",
                          gap: 1,
                          "&:hover": {
                            color: "rgb(200, 200, 200)",
                            backgroundColor: "rgb(60, 60, 60)",
                          },
                        }}
                        onClick={() => dispatch(addToCart(product))}
                      >
                        <Box component={"div"} onClick={handleClick()}>
                          <ShoppingCartIcon /> Agregar al carrito
                        </Box>
                      </Button>
                    </Box>
                  </CardContent>
                </Card>
              </Box>
            </Grid>
          </Box>
        ))}
      </Grid>
    </Fragment>
  );
}

export default HomeApplianceTotalComponent;
