import { Box, Typography } from "@mui/material";
import React, { Fragment } from "react";

function Footer() {
  return (
    <Fragment>
      <Box
        sx={{
          width: "100%",
          height: "115px",
          maxWidth: "100%",
          background: "rgb(25, 25, 25);",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          backgroundAttachment: "fixed",
          backgroundPosition: "center",
          alignItems: "center",
          justifyContent: "center",
          display: "flex",
          color: "gray",
          textAlign: "center",
          paddingLeft: { xs: "20px" },
          paddingRight: { xs: "20px" },
          paddingTop: { xs: "23px" },
          paddingBottom: { xs: "23px" },
          padding: { md: "35px", lg: "35px", xl: "35px" },
        }}
      >
        <Box
          sx={{
            wordWrap: "break-word",
            overflowWrap: "break-word",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Typography
            gutterBottom
            variant="h7"
            sx={{ fontSize: { xs: 13, sm: 14, md: 15, lg: 16, xl: 17 } }}
          >
            E-commerce © Copyright 2024 e-commerce | All Rights Reserved | This
            site designed and hosted by Enter.Net. Our Privacy Policy and Terms
            of Service apply.
          </Typography>
        </Box>
      </Box>
    </Fragment>
  );
}

export default Footer;
