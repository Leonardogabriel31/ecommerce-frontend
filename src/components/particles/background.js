"use client";
import { Box } from "@mui/material";
import ParticlesConfig from "./config/particles-config";

export default function Background() {
  return (
    <Box component={"div"}>
      <ParticlesConfig id="particles" />
    </Box>
  );
}
